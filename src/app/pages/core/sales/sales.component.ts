import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { PrivateService } from 'src/app/services/applications/private.service';
import { ApplicationsSalesService } from 'src/app/services/applications/applications-sales.service';
import { ApplicationsService } from 'src/app/services/applications/applications.service';
import { CurrenciesService } from 'src/app/services/currencies/currencies.service';

@Component({
  selector: 'app-sales',
  templateUrl: './sales.component.html',
  styleUrls: ['./sales.component.css']
})
export class SalesComponent implements OnInit {
  page: number = 1;
  isCollapsed:boolean=false;  
  data: any;
  constructor(private salesService: ApplicationsSalesService, public privateService: PrivateService) {
  }

  ngOnInit() {
    this.refreshItemsList();
      this.data = {
            title: "Pagos",
            service: this.salesService,
            filter:true
      }            
  }

  refreshItemsList() {
    this.privateService.refreshItemsList(this.salesService, this.page);
  }
}
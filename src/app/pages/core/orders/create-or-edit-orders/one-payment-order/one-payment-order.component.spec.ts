import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OnePaymentOrderComponent } from './one-payment-order.component';

describe('OnePaymentOrderComponent', () => {
  let component: OnePaymentOrderComponent;
  let fixture: ComponentFixture<OnePaymentOrderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OnePaymentOrderComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OnePaymentOrderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateOrEditOrdersComponent } from './create-or-edit-orders.component';

describe('CreateOrEditOrdersComponent', () => {
  let component: CreateOrEditOrdersComponent;
  let fixture: ComponentFixture<CreateOrEditOrdersComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreateOrEditOrdersComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateOrEditOrdersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

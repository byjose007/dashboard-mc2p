import { Component, OnInit } from '@angular/core';
import { ApplicationsService } from 'src/app/services/applications/applications.service';
import { PrivateService } from 'src/app/services/applications/private.service';
import { ApplicationsSubscriptionsService } from 'src/app/services/applications/applications-subscriptions.service';
import { ApplicationsOrdersService } from '../../../services/applications/applications-orders.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-orders',
  templateUrl: './orders.component.html',
  styleUrls: ['./orders.component.css']
})
export class OrdersComponent implements OnInit {
  data: any;
  page: number = 1;

  constructor(private router: Router, private ordersService: ApplicationsOrdersService,
    public privateService: PrivateService) { }
  ngOnInit() {
    this.refreshItemsList();
    this.data = {
      title: "Òrdenes",
      service: this.ordersService,
      filter: true
    }
  }

  refreshItemsList() {
    this.privateService.refreshItemsList(this.ordersService, this.page);
  }


  edit(item) {
    this.privateService.setItemModel(item);
    // this.dataActions.redirectTo.push(item.id);
    if(item.order_type == 'S')
      this.router.navigate([`core/orders/createOrEdit/${item.id}`]);
  }

  delete(item) {
    // this.openDialog();
    // this.privateService.delete(item, this.dataActions.service);
  }
}

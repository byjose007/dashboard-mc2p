import { Component, OnInit } from '@angular/core';
import { PrivateService } from 'src/app/services/applications/private.service';
import { Router, ActivatedRoute } from '@angular/router';
import { ApplicationsService } from 'src/app/services/applications/applications.service';
import { ApplicationsTransactionsService } from '../../../services/applications/applications-transactions.service';
import { ClipboardModule } from 'ngx-clipboard';

@Component({
  selector: 'app-transactions',
  templateUrl: './transactions.component.html',
  styleUrls: ['./transactions.component.css']
})
export class TransactionsComponent implements OnInit {
  data: any;
  page=1;

  constructor(private transactionsService: ApplicationsTransactionsService, private privateService: PrivateService) { }


  ngOnInit() {
    this.refreshItemsList();
    this.data = {
      title: "Suscripciones",
      service: this.transactionsService,
      filter:true
    }
  }


  refreshItemsList() {
    this.privateService.refreshItemsList(this.transactionsService, this.page);
  }
}


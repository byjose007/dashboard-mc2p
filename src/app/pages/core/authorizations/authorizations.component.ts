import { Component, OnInit } from '@angular/core';
import { ApplicationsService } from 'src/app/services/applications/applications.service';
import { PrivateService } from 'src/app/services/applications/private.service';
import { ApplicationsAuthorizationsService } from 'src/app/services/applications/applications-authorizations.service';

@Component({
  selector: 'app-authorizations',
  templateUrl: './authorizations.component.html',
  styleUrls: ['./authorizations.component.css']
})
export class AuthorizationsComponent implements OnInit {
  data: any;
  page: number = 1;


  constructor(private authorizationsService: ApplicationsAuthorizationsService, public privateService: PrivateService) { }

  ngOnInit() {
    this.refreshItemsList();
    this.data = {
      title: "Autorizaciones",
      service: this.authorizationsService,
      filter:true
    }
  }

  refreshItemsList() {
    this.privateService.refreshItemsList(this.authorizationsService, this.page);
  }


}

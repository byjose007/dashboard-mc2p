import { Component, OnInit } from '@angular/core';
import { ApplicationsService } from 'src/app/services/applications/applications.service';
import { PrivateService } from 'src/app/services/applications/private.service';
import { ApplicationsSubscriptionsService } from 'src/app/services/applications/applications-subscriptions.service';

@Component({
  selector: 'app-subscriptions',
  templateUrl: './subscriptions.component.html',
  styleUrls: ['./subscriptions.component.css']
})
export class SubscriptionsComponent implements OnInit {
  data:any;
  page: number = 1;
  constructor( private subcriptionsService: ApplicationsSubscriptionsService, private privateService: PrivateService
    ) { }



  ngOnInit() {
    this.refreshItemsList();
      this.data = {
            title: "Suscripciones",
            service: this.subcriptionsService,
            filter:true
      }            
  }

  refreshItemsList() {
    this.privateService.refreshItemsList(this.subcriptionsService, this.page);
  }

}





import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';

import { PagesComponent } from './pages.component';
import { ListComponent } from './list/list.component';
import { RetrieveComponent } from './retrieve/retrieve.component';
import { SalesComponent } from './core/sales/sales.component';


import { TransactionsComponent } from './core/transactions/transactions.component';
import { GatewaysComponent } from './settings/gateways/gateways.component';
import { GeneralComponent } from './stats/general/general.component';

import { SubscriptionsComponent } from './core/subscriptions/subscriptions.component';
import { AuthorizationsComponent } from 'src/app/pages/core/authorizations/authorizations.component';
import { OrdersComponent } from './core/orders/orders.component';
import { ProductsComponent } from './items/products/products.component';
import { PlansComponent } from './items/plans/plans.component';
import { CouponsComponent } from './items/coupons/coupons.component';
import { TaxesComponent } from './settings/taxes/taxes.component';
import { ShippingsComponent } from './settings/shippings/shippings.component';
import { WebhooksComponent } from './settings/webhooks/webhooks.component';
import { CreateOrEditProductsComponent } from './items/products/create-or-edit-products/create-or-edit-products.component';
import { CreateOrEditPlansComponent } from './items/plans/create-or-edit-plans/create-or-edit-plans.component';
import { CreateOrEditCouponsComponent } from './items/coupons/create-or-edit-coupons/create-or-edit-coupons.component';
import { CreateOrEditTaxesComponent } from './settings/taxes/create-or-edit-taxes/create-or-edit-taxes.component';
import { CreateOrEditShippingsComponent } from './settings/shippings/create-or-edit-shippings/create-or-edit-shippings.component';
import { CreateOrEditWebhooksComponent } from './settings/webhooks/create-or-edit-webhooks/create-or-edit-webhooks.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { ApplicationsComponent } from './applications/applications.component';



const pagesRoutes: Routes = [
  { path: '', component: ListComponent },
  { path: 'new', component: ApplicationsComponent },
  { path: ':id', component: RetrieveComponent },
  { path: 'core/sales', component: SalesComponent },
  { path: 'core/subscriptions', component: SubscriptionsComponent },
  { path: 'core/authorizations', component: AuthorizationsComponent },
  { path: 'core/transactions', component: TransactionsComponent },
  { path: 'core/orders', component: OrdersComponent },
  { path: 'core/orders/createOrEdit/:id', component: DashboardComponent },


  { path: 'items/products', component: ProductsComponent },
  { path: 'items/products/createOrEdit/:id', component: CreateOrEditProductsComponent },
  
  { path: 'items/plans', component: PlansComponent },
  { path: 'items/plans/createOrEdit/:id', component: CreateOrEditPlansComponent },

  { path: 'items/coupons', component: CouponsComponent },
  { path: 'items/coupons/createOrEdit/:id', component: CreateOrEditCouponsComponent },

  { path: 'stats/general', component: GeneralComponent },

  { path: 'settings/gateway', component: GatewaysComponent },
  { path: 'settings/taxes', component: TaxesComponent },
  { path: 'settings/taxes/createOrEdit/:id', component: CreateOrEditTaxesComponent },

  { path: 'settings/shippings', component: ShippingsComponent },
  { path: 'settings/shippings/createOrEdit/:id', component: CreateOrEditShippingsComponent },

  { path: 'settings/webhooks', component: WebhooksComponent },
  { path: 'settings/webhooks/createOrEdit/:id', component:CreateOrEditWebhooksComponent },
  // { path: '', redirectTo: '/list', pathMatch: 'full' },

];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(pagesRoutes) 
    
  ],
  exports: [ RouterModule ],
  declarations: []
})
export class PagesRoutingModule { }


import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { PrivateService } from 'src/app/services/applications/private.service';

import { ActivatedRoute, Router } from '@angular/router';
import { ApplicationsService } from 'src/app/services/applications/applications.service';
import { ValidateService } from 'src/app/services/validate/validate.service';

@Component({
  selector: 'app-applications',
  templateUrl: './applications.component.html',
})
export class ApplicationsComponent implements OnInit {
  item: any = {};
  data: any = {};
  applicationForm: FormGroup;
  errors: any[] = [];

  constructor(public privateService: PrivateService, private formBuilder: FormBuilder,
    private applicationsService: ApplicationsService, private route: ActivatedRoute,
    private validateService: ValidateService, private router: Router) {
      this.createForm();
  }

  ngOnInit() {

  }


  createForm() {
    this.applicationForm = this.formBuilder.group({
      name: [this.item.name, [Validators.required]],
      url: [this.item.url],
      test: [this.item.test || false],
      logo: [this.item.logo]
    });


  }

  save() {
    this.errors = [];
    this.validateService.formValidation(this.applicationForm, this.errors);
    if (this.applicationForm.valid) {
      this.applicationsService.createApplication(this.applicationForm.value).subscribe(
        () => {
          this.router.navigate(['/']);
        }, (e) => {
          this.errors = e.error;
        });
    }
  }
  back(){
    this.router.navigate(['/']);

  }

  delete(id) {
      this.applicationsService.deleteApplication(id)
      this.router.navigate(['/']);

    }
  }



}

import { Component, OnInit } from '@angular/core';
import { ApplicationsService } from 'src/app/services/applications/applications.service';
import { PrivateService } from '../../../services/applications/private.service';

@Component({
  selector: 'app-gateways',
  templateUrl: './gateways.component.html',
  styleUrls: ['./gateways.component.css']
})
export class GatewaysComponent implements OnInit {
  usedGatewayList: any;
  availableGatewayList: any;

  constructor(private applicationsService: ApplicationsService, private privateService: PrivateService) {}

  ngOnInit() {
    this.applicationsService.getApplicationPayments(this.privateService.application.id, { used: 0 }).subscribe(
      (data: any) => this.usedGatewayList = data);

    this.applicationsService.getApplicationPayments(this.privateService.application.id).subscribe(
      (data: any) => this.availableGatewayList = data);
  }
}



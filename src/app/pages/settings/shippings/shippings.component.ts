import { Component, OnInit } from '@angular/core';
import { PrivateService } from 'src/app/services/applications/private.service';
import { ApplicationsShippingsService } from 'src/app/services/applications/applications-shippings.service';

@Component({
  selector: 'app-shippings',
  templateUrl: './shippings.component.html',
  styleUrls: ['./shippings.component.css']
})
export class ShippingsComponent implements OnInit {
  data: any;
  page = 1;
  dataActions:any;

  constructor( public privateService: PrivateService, private shippingsService: ApplicationsShippingsService) { }
  ngOnInit() {
    this.refreshItemsList();
    this.data = {
      title: "Envíos",
      service: this.shippingsService,
      filter: false
    }

    this.dataActions ={
      buttons: ['edit','delete'],
      redirectTo: ['','settings','shippings','createOrEdit'],
      service: this.shippingsService,
    }
  }

  refreshItemsList() {
    this.privateService.refreshItemsList(this.shippingsService, this.page);
  }
}

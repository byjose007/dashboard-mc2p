import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { PrivateService } from 'src/app/services/applications/private.service';
import { ActivatedRoute } from '@angular/router';
import { ApplicationsShippingsService } from 'src/app/services/applications/applications-shippings.service';

@Component({
  selector: 'app-create-or-edit-shippings',
  templateUrl: './create-or-edit-shippings.component.html',
})
export class CreateOrEditShippingsComponent implements OnInit {
  item: any = {};
  data: any;
  public shippingsForm: FormGroup;

  constructor(public privateService: PrivateService, private formBuilder: FormBuilder,
    private shippingsService: ApplicationsShippingsService, private route: ActivatedRoute) {

    if (this.route.snapshot.paramMap.get('id')) {
      this.item = this.privateService.getItemModel();
      this.createForm();
    } else
      this.createForm();
   }

  ngOnInit() {
  }

  createForm() {
    this.shippingsForm = this.formBuilder.group({
      id: [this.item.id],      
      name: [this.item.name, [Validators.required]],
      price: [this.item.price, [Validators.required]],

    });

    this.data = {
      title: "Envío",
      redirectTo: ['', 'settings', 'shippings'],
      form: this.shippingsForm,
      service: this.shippingsService
    }

  }
}
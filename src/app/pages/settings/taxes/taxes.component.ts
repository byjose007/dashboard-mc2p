import { Component, OnInit } from '@angular/core';
import { PrivateService } from 'src/app/services/applications/private.service';
import { ApplicationsTransactionsService } from '../../../services/applications/applications-transactions.service';
import { ApplicationsTaxesService } from '../../../services/applications/applications-taxes.service';

@Component({
  selector: 'app-taxes',
  templateUrl: './taxes.component.html',
  styleUrls: ['./taxes.component.css']
})
export class TaxesComponent implements OnInit {
  data: any;
  page = 1;
  dataActions:any;

  constructor( public privateService: PrivateService, private taxesService: ApplicationsTaxesService) { }
  ngOnInit() {
    this.refreshItemsList();
    this.data = {
      title: "Impuestos",
      service: this.taxesService,
      filter: false
    }

    this.dataActions ={
      buttons: ['edit','delete'],
      redirectTo: ['','settings','taxes','createOrEdit'],
      service: this.taxesService,
    }
  }


  refreshItemsList() {
    this.privateService.refreshItemsList(this.taxesService, this.page);
  }
}

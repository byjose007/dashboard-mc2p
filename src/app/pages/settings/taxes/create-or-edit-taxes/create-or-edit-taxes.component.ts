import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { PrivateService } from 'src/app/services/applications/private.service';
import { ActivatedRoute } from '@angular/router';
import { ApplicationsTaxesService } from 'src/app/services/applications/applications-taxes.service';

@Component({
  selector: 'app-create-or-edit-taxes',
  templateUrl: './create-or-edit-taxes.component.html',

})
export class CreateOrEditTaxesComponent implements OnInit {
  item: any = {};
  data: any;
  public taxesForm: FormGroup;

  constructor(public privateService: PrivateService, private formBuilder: FormBuilder,
    private taxesService: ApplicationsTaxesService, private route: ActivatedRoute) {

    if (this.route.snapshot.paramMap.get('id')) {
      this.item = this.privateService.getItemModel();
      this.createForm();
    } else
      this.createForm();
   }

  ngOnInit() {
  }

  createForm() {
    this.taxesForm = this.formBuilder.group({
      id: [this.item.id],      
      name: [this.item.name, [Validators.required]],
      percent: [this.item.percent,[Validators.required]],

    });

    this.data = {
      title: "Impuestos",
      redirectTo: ['', 'settings', 'taxes'],
      form: this.taxesForm,
      service: this.taxesService
    }

  }

}

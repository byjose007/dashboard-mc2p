import { Component, OnInit } from '@angular/core';
import { PrivateService } from 'src/app/services/applications/private.service';
import { ActivatedRoute } from '@angular/router';
import { ApplicationsWebhooksService } from 'src/app/services/applications/applications-webhooks.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';


@Component({
  selector: 'app-create-or-edit-webhooks',
  templateUrl: './create-or-edit-webhooks.component.html',
})
export class CreateOrEditWebhooksComponent implements OnInit {
  item: any = {};
  data: any;
  webhooksForm: FormGroup;
  TypeChoices:any;

  constructor(public privateService: PrivateService, private formBuilder: FormBuilder,
    private webhooksService: ApplicationsWebhooksService, private route: ActivatedRoute) {

    if (this.route.snapshot.paramMap.get('id')) {
      this.item = this.privateService.getItemModel();
      this.createForm();
    } else
      this.createForm();
   }

  ngOnInit() {
    this.TypeChoices = this.webhooksService.getWebHookTypeChoices();
  }

  createForm() {
    this.webhooksForm = this.formBuilder.group({
      id: [this.item.id],      
      name: [this.item.name, [Validators.required]],
      url: [this.item.url,[Validators.required]],
      web_hook_type: [this.item.web_hook_type,[Validators.required]],

    });

    this.data = {
      title: "Web hooks",
      redirectTo: ['', 'settings', 'webhooks'],
      form: this.webhooksForm,
      service: this.webhooksService
    }

  }
}
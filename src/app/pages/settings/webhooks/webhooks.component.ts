import { Component, OnInit } from '@angular/core';
import { PrivateService } from 'src/app/services/applications/private.service';
import { ApplicationsWebhooksService } from 'src/app/services/applications/applications-webhooks.service';

@Component({
  selector: 'app-webhooks',
  templateUrl: './webhooks.component.html',
  styleUrls: ['./webhooks.component.css']
})
export class WebhooksComponent implements OnInit {
  data: any;
  page = 1;
  dataActions:any;

  constructor( public privateService: PrivateService, private webhooksService: ApplicationsWebhooksService) { }
  ngOnInit() {
    this.refreshItemsList();
    this.data = {
      title: "Web Hooks",
      service: this.webhooksService,
      filter: false
    }

    this.dataActions ={
      buttons: ['edit','delete'],
      redirectTo: ['','settings','webhooks','createOrEdit'],
      service: this.webhooksService,
    }
  }


  refreshItemsList() {
    this.privateService.refreshItemsList(this.webhooksService, this.page);
  }
}

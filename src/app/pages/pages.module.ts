import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BrowserModule } from '@angular/platform-browser';
import { ChartsModule } from 'ng2-charts/charts/charts';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import { ClipboardModule } from 'ngx-clipboard';
import { RouterModule } from '@angular/router';



// Rutas


// Modulos
import { SharedModule } from '../shared/shared.module';

// Componentes
import { RetrieveComponent } from './retrieve/retrieve.component';
import { SubscriptionsComponent } from './core/subscriptions/subscriptions.component';
import { TransactionsComponent } from './core/transactions/transactions.component';
import { OrdersComponent } from './core/orders/orders.component';
import { SalesComponent } from './core/sales/sales.component';
import { GatewaysComponent } from 'src/app/pages/settings/gateways/gateways.component';
import { GeneralComponent } from './stats/general/general.component';
import { AuthorizationsComponent } from './core/authorizations/authorizations.component';
import { DataDableComponent } from '../components/dataTable/data-dable.component';
import { CoreItemSalesStatusComponent } from 'src/app/components/core-intem-sales-status/core-item-sales-status.component';
import { CoreItemSalesComponent } from 'src/app/components/core-item-sales/core-item-sales.component';
import { CoreIntemComponent } from 'src/app/components/core-item/core-item.component';
import { FiltersComponent } from 'src/app/components/dataTable/filters/filters.component';
import { PagesComponent } from './pages.component';
import { PagesRoutingModule } from 'src/app/pages/pages-routing.module';
import { DashboardComponent } from 'src/app/pages/dashboard/dashboard.component';
import { TaxesComponent } from './settings/taxes/taxes.component';
import { ShippingsComponent } from './settings/shippings/shippings.component';
import { WebhooksComponent } from './settings/webhooks/webhooks.component';
import { ProductsComponent } from 'src/app/pages/items/products/products.component';
import { PlansComponent } from './items/plans/plans.component';
import { CouponsComponent } from './items/coupons/coupons.component';
import { ListComponent } from 'src/app/pages/list/list.component';


// Services
import { PrivateService } from '../services/applications/private.service';
import { ApplicationsSalesService } from '../services/applications/applications-sales.service';
import { GenericService } from '../services/generic.service';
import { WrapperService } from '../services/wrapper/wrapper.service';
import { ApplicationsSubscriptionsService } from '../services/applications/applications-subscriptions.service';
import { ApplicationsTransactionsService } from '../services/applications/applications-transactions.service';
import { ApplicationsStatisticsService } from '../services/applications/applications-statistics.service';
import { ApplicationsService } from 'src/app/services/applications/applications.service';

//Create or Edit
import { CreateOrEditProductsComponent } from './items/products/create-or-edit-products/create-or-edit-products.component';
import { CreateOrEditPlansComponent } from './items/plans/create-or-edit-plans/create-or-edit-plans.component';
import { CreateOrEditComponent } from 'src/app/components/create-or-edit/create-or-edit.component';
import { CreateOrEditCouponsComponent } from './items/coupons/create-or-edit-coupons/create-or-edit-coupons.component';
import { ActionsButtonsComponent } from 'src/app/components/actions-buttons/actions-buttons.component';
import { CreateOrEditTaxesComponent } from './settings/taxes/create-or-edit-taxes/create-or-edit-taxes.component';
import { CreateOrEditShippingsComponent } from 'src/app/pages/settings/shippings/create-or-edit-shippings/create-or-edit-shippings.component';
import { CreateOrEditWebhooksComponent } from 'src/app/pages/settings/webhooks/create-or-edit-webhooks/create-or-edit-webhooks.component';
import { CreateOrEditOrdersComponent } from './core/orders/create-or-edit-orders/create-or-edit-orders.component';
import { OnePaymentOrderComponent } from './core/orders/create-or-edit-orders/one-payment-order/one-payment-order.component';
import { SubscriptionOrderComponent } from 'src/app/pages/core/orders/create-or-edit-orders/subscription-order/subscription-order.component';
import { ApplicationsComponent } from './applications/applications.component';
import { CreateOrEditWatewaysComponent } from './settings/create-or-edit-wateways/create-or-edit-wateways.component';







@NgModule({
    declarations: [
        DashboardComponent,
        ListComponent,
        // PagesComponent,
        RetrieveComponent,
        SalesComponent,
   
        SubscriptionsComponent,
        TransactionsComponent,
        OrdersComponent,

        CoreItemSalesComponent,
        CoreItemSalesStatusComponent,
        CoreIntemComponent,
        GatewaysComponent,
        GeneralComponent,
        
        AuthorizationsComponent,
        DataDableComponent,
        FiltersComponent,
        ProductsComponent,
        PlansComponent,
        CouponsComponent,
        TaxesComponent,
        ShippingsComponent,
        WebhooksComponent,
        ActionsButtonsComponent,
   
        
        CreateOrEditProductsComponent,
        CreateOrEditPlansComponent,
        CreateOrEditComponent,
        CreateOrEditCouponsComponent,
        CreateOrEditTaxesComponent,
        CreateOrEditShippingsComponent,
        CreateOrEditWebhooksComponent,
        CreateOrEditOrdersComponent,
        SubscriptionOrderComponent,
        OnePaymentOrderComponent,
        ApplicationsComponent,
        CreateOrEditWatewaysComponent,

        
       

    ],
    // entryComponents: [DialogOverviewExampleDialog],
    exports: [
        // PagesComponent,
        DashboardComponent,
        ListComponent,
        RetrieveComponent,
        SalesComponent,
        GeneralComponent,        
        GatewaysComponent,
        ProductsComponent,
        DataDableComponent,
        FiltersComponent,
        TaxesComponent,
        ShippingsComponent,
        WebhooksComponent,
        CreateOrEditComponent
        

    ],
    imports: [
        
        // BrowserModule,
        PagesRoutingModule,       
        SharedModule,
        CommonModule,
        ChartsModule,
        FormsModule,
        ReactiveFormsModule,
        ClipboardModule,     
        NgbModule,
   
        

    ],
    providers: [
        GenericService,
        ApplicationsService,
        ApplicationsStatisticsService,
        ApplicationsSalesService,
        ApplicationsTransactionsService,
        ApplicationsSubscriptionsService,
        PrivateService,
        WrapperService
    ]
})
export class PagesModule { }

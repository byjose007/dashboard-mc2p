import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { PrivateService } from 'src/app/services/applications/private.service';
import { ApplicationsProductsService } from 'src/app/services/applications/applications-products.service';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-create-or-edit-products',
  templateUrl: './create-or-edit-products.component.html',

})
export class CreateOrEditProductsComponent implements OnInit {
  item: any = {};
  data: any = {};
  productForm: FormGroup;

  constructor(public privateService: PrivateService, private formBuilder: FormBuilder,
    private productsService: ApplicationsProductsService, private route: ActivatedRoute) {

    if (this.route.snapshot.paramMap.get('id')) {
      this.item = this.privateService.getItemModel();
      this.createForm();
    } else
      this.createForm();
  }

  ngOnInit() {

  }


  createForm() {
    this.productForm = this.formBuilder.group({
      name: [this.item.name, [Validators.required]],
      product_id: [this.item.product_id],
      price: [this.item.price, [Validators.required]],
      description: [this.item.description],
      id: [this.item.id]
    });

    this.data = {
      title: "Planes",
      redirectTo: ['', 'items', 'products'],
      form: this.productForm,
      service: this.productsService
    }
  }


}

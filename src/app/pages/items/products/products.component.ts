import { Component, OnInit, Inject} from '@angular/core';
import { PrivateService } from 'src/app/services/applications/private.service';
import { ApplicationsProductsService } from 'src/app/services/applications/applications-products.service';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { Router } from '@angular/router';

import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';


@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.css']
})
export class ProductsComponent implements OnInit {
  data: any;
  page=1;
  dataActions:any;

  constructor(private router: Router, private productsService: ApplicationsProductsService,
     public privateService: PrivateService, private modalService: NgbModal,private dialog: MatDialog) {}

  ngOnInit() {
    this.refreshItemsList();
    this.data = {
      title: "Productos",
      service: this.productsService,
      filter:false
    }


    this.dataActions ={
      buttons: ['edit','delete'],
      redirectTo: ['','items','products','createOrEdit'],
      service: this.productsService,
    }
  }


  refreshItemsList() {
    this.privateService.refreshItemsList(this.productsService, this.page);
  }

}

import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { PrivateService } from 'src/app/services/applications/private.service';
import { ActivatedRoute } from '@angular/router';
import { ApplicationsCouponsService } from 'src/app/services/applications/applications-coupons.service';

@Component({
  selector: 'app-create-or-edit-coupons',
  templateUrl: './create-or-edit-coupons.component.html',

})
export class CreateOrEditCouponsComponent implements OnInit {
  item: any = {};
  data: any = {};
  couponForm: FormGroup;
  TypeChoices:any;

  constructor(public privateService: PrivateService, private formBuilder: FormBuilder,
    private couponsService: ApplicationsCouponsService, private route: ActivatedRoute) {

    if (this.route.snapshot.paramMap.get('id')) {
      this.item = this.privateService.getItemModel();
      this.createForm();
    } else
      this.createForm();
  }

  ngOnInit() {
    this.TypeChoices = this.couponsService.getCouponTypeChoices();
  }


  createForm() {
    this.couponForm = this.formBuilder.group({
      id: [this.item.id],
      coupon_id: [this.item.coupon_id],
      name: [this.item.name, [Validators.required]],     
      code: [this.item.code, [Validators.required]],
      coupon_type: [this.item.coupon_type,[Validators.required]],
      value: [this.item.value,[Validators.required]],
      expiration_date: [this.item.expiration_date],
      total_uses: [this.item.total_uses,[Validators.required]],
      apply_to_plans: [this.item.apply_to_plans || false],
      apply_to_products: [this.item.apply_to_products || false],
    })


    this.data = {
      title: "Copones",
      redirectTo: ['', 'items', 'coupons'],
      form: this.couponForm,
      service: this.couponsService
    }
  }


}

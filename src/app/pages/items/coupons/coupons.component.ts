import { Component, OnInit } from '@angular/core';
import { PrivateService } from 'src/app/services/applications/private.service';
import { ApplicationsCouponsService } from 'src/app/services/applications/applications-coupons.service';

@Component({
  selector: 'app-coupons',
  templateUrl: './coupons.component.html',
  styleUrls: ['./coupons.component.css']
})
export class CouponsComponent implements OnInit {
  data: any;
  page=1;
  dataActions:any;

  constructor(private couponsService: ApplicationsCouponsService, public privateService: PrivateService) { }


  ngOnInit() {
    this.refreshItemsList();
    this.data = {
      title: "Cupones",
      service: this.couponsService,
      filter:false
    }

    this.dataActions ={
      buttons: ['edit','delete'],
      redirectTo: ['','items','coupons','createOrEdit'],
      service: this.couponsService,
    }
  }


  refreshItemsList() {
    this.privateService.refreshItemsList(this.couponsService, this.page);
  }
}

import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { PrivateService } from 'src/app/services/applications/private.service';
import { ActivatedRoute } from '@angular/router';
import { ApplicationsPlansService } from 'src/app/services/applications/applications-plans.service';


@Component({
  selector: 'app-create-or-edit-plans',
  templateUrl: './create-or-edit-plans.component.html',
})
export class CreateOrEditPlansComponent implements OnInit {

  item: any = {};
  data: any;
  public PlanForm: FormGroup;

  constructor(public privateService: PrivateService, private formBuilder: FormBuilder,
    private plansService: ApplicationsPlansService, private route: ActivatedRoute) {

    if (this.route.snapshot.paramMap.get('id')) {
      this.item = this.privateService.getItemModel();
      this.createForm();
    } else
      this.createForm();
  }

  ngOnInit() { }


  createForm() {
    this.PlanForm = this.formBuilder.group({
      id: [this.item.id],
      plan_id: [this.item.plan_id],
      name: [this.item.name, [Validators.required]],
      price: [this.item.price, [Validators.required]],
      description: [this.item.description],
      duration: [this.item.duration, [Validators.required]],
      unit: [this.item.unit, [Validators.required]],
      recurring: [this.item.recurring]

    });

    this.data = {
      title: "Planes",
      redirectTo: ['', 'items', 'plans'],
      form: this.PlanForm,
      service: this.plansService
    }

  }

}

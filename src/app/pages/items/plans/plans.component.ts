import { Component, OnInit } from '@angular/core';
import { PrivateService } from 'src/app/services/applications/private.service';
import { ApplicationsPlansService } from 'src/app/services/applications/applications-plans.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-plans',
  templateUrl: './plans.component.html',
  styleUrls: ['./plans.component.css']
})
export class PlansComponent implements OnInit {

  data: any;
  page = 1;
  dataActions:any;

  constructor(private plansService: ApplicationsPlansService,
    private router: Router, public privateService: PrivateService) { }


  ngOnInit() {
    this.refreshItemsList();
    this.data = {
      title: "Planes",
      service: this.plansService,
      filter: false
    }

    this.dataActions ={
      buttons: ['edit','delete'],
      redirectTo: ['','items','plans','createOrEdit'],
      service: this.plansService,
    }
  }


  refreshItemsList() {
    this.privateService.refreshItemsList(this.plansService, this.page);
  }


}

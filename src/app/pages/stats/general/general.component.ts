import { Component, OnInit } from '@angular/core';
import { ApplicationsStatisticsService } from 'src/app/services/applications/applications-statistics.service';
import { ApplicationsService } from 'src/app/services/applications/applications.service';
import * as Moment from 'moment';
import { ActivatedRoute } from '@angular/router';
import { PrivateService } from '../../../services/applications/private.service';
import { isObject } from 'util';

@Component({
  selector: 'app-general',
  templateUrl: './general.component.html',
  styleUrls: ['./general.component.css']
})
export class GeneralComponent implements OnInit {
  dateMin:any;
  dateMax:any;
  totalNGateways:any;
  salesCount:any = {};
  salesValue:any;
  value_labels:any;
  countLabels:any;
  salesCountData:any;
  salesValueData:any;
  totalData:any;
  totalNGatewaysGraph:any;
  id:any;

  constructor(private applicationsService: ApplicationsService, private privateService: PrivateService,
    private statisticsService: ApplicationsStatisticsService, private route: ActivatedRoute) {
    this.dateMin = Moment().subtract(1, 'year').format('MM/D/YYYY');
    this.dateMax = Moment().format('MM/D/YYYY');
    this.totalNGateways = { period: 'month', activeTab: 0 };
    this.salesCount = { period: 'month' };
    this.salesValue = { period: 'month' };
     }

  ngOnInit() {
    // this.id = this.route.snapshot.paramMap.get('id');
    this.id = this.privateService.application.id;
    this.refreshGraphs();
  }


    getParams(period:any) {
      let params = {};
      // if(isObject(this.dateMin)){
      //   params['min_date'] = `${this.dateMin.day}/${this.dateMin.month}/${this.dateMin.year}`;   
      // } 
      // if(isObject(this.dateMax)){
      //   params['max_date'] = `${this.dateMax.day}/${this.dateMax.month}/${this.dateMax.year}`;
      // }
      if (this.dateMin && this.dateMax) {
        if (Moment(this.dateMax).diff(Moment(this.dateMin), 'days') <= 60) {
          period = 'day';
        } else {
          period = 'month';
        }
      } else {
        params = this.statisticsService.buildParamsFromPeriod(period);
      }
     
     
      if (this.dateMin) {
        params['min_date'] = this.dateMin;
      }
      if (this.dateMax) {
        params['max_date'] = this.dateMax;
      }
      return params;
    };

    getSalesValueGraph  () {
      let params:any = this.getParams(this.salesValue.period);
      this.salesValue.period = params['period'];

      this.statisticsService.getApplicationSalesValueGraph(this.id, params)
        .subscribe( (response:any) => {
          this.salesValue.graph = response;
          this.salesValueData = this.getDataChart(this.salesValue.graph.data, this.salesValue.graph.series);
        });
    };

    getSalesCountGraph  () {
      let params:any = this.getParams(this.salesCount.period);
      this.salesCount.period = params['period'];

      this.statisticsService.getApplicationSalesCountGraph(this.id, params)
      .subscribe( (response:any) => {
          this.salesCount.graph = response;
          this.salesCountData = this.getDataChart(this.salesCount.graph.data, this.salesCount.graph.series);
        });
    };

    getTotalNGatewaysGraph  () {
      let params:any = this.getParams(this.totalNGateways.period);
      this.totalNGateways.period = params['period'];

      this.statisticsService.getGeneralTotalNGatewaysValueGraph(this.id, params)
      .subscribe( response => {
          this.totalNGateways.graph = response;
          this.totalData = this.getDataChart(this.totalNGateways.graph.data, this.totalNGateways.graph.series);
          this.onTotalNGatewaysTabClick();
        });
    };

    onTotalNGatewaysTabClick(index:any=0) {
      let series:any[] = [];
      let data:any[] = [];
      if (index) {
        series = [this.totalNGateways.graph.series[index]];
        data = [this.totalNGateways.graph.data[index]];
        this.totalData = this.getDataChart(data,series);
      } else {
        series = this.totalNGateways.graph.series;
        data = this.totalNGateways.graph.data;
        this.totalData = this.getDataChart(data,series);
      }

      this.totalNGateways.activeTab = index;
      this.totalNGatewaysGraph = {
        series: series,
        data: data
      };
    };

    refreshGraphs(){
      this.getSalesValueGraph();
      this.getSalesCountGraph();
      this.getTotalNGatewaysGraph();
    };


    getDataChart(arrayObject:any, series:any){    
      let data:any[] = [];    
      arrayObject.forEach((element, index) => {     
        // console.log(element) 
        data.push({
             data: element, 
            label: series[index],
          })
      });
      return data;
    }

 
  // }


}

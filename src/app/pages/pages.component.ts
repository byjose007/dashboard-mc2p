import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { PrivateService } from 'src/app/services/applications/private.service';

@Component({
  selector: 'app-pages',
  templateUrl: './pages.component.html',
  styles: []
})
export class PagesComponent implements OnInit {
  submenu: string = '';

  constructor(private route: ActivatedRoute, private privateService: PrivateService) {

  }


  ngOnInit() {
  }





  cargarSubmenu(nombre) {
    this.submenu = nombre;
  }

}


import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Location } from '@angular/common';
import {Chart} from 'chart.js';
import { ApplicationsStatisticsService } from 'src/app/services/applications/applications-statistics.service';
import { ApplicationsService } from 'src/app/services/applications/applications.service';
import { PrivateService } from 'src/app/services/applications/private.service';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-retrieve',
  templateUrl: './retrieve.component.html',
  styleUrls: ['./retrieve.component.css']
})
export class RetrieveComponent implements OnInit {
  //applicationsModel; = applicationsModel;
  //applicationTotals; = applicationTotals;

  applicationsModel: any;
  
  applicationTotals: any;
  totalNGateways: any;
  transactionsNSales: any;
  totalNGatewaysGraph: any;
  id: any;
  chart:any = []; 
  showSubmenu:boolean;
  application:any;
  @Output() applicationData = new EventEmitter();


  constructor(
    private route: ActivatedRoute, 
    private location: Location,
    private router: Router,
    private statisticsService: ApplicationsStatisticsService,
    private applicationsService: ApplicationsService,
    private privateService: PrivateService,
    private modalService: NgbModal) {
    this.totalNGateways = { period: 'year', activeTab: 0 };
    this.transactionsNSales = { period: 'year' };
    this.showSubmenu = false;
    this.application = {};
  }

  ngOnInit() {
    console.log(this.router.routerState);    
    this.id = this.route.snapshot.paramMap.get('id');
    this.getTotalValues(this.id);
    this.getApplication(this.id);
    this.getTotalNGatewaysGraph();
    this.getTransactionsNSalesGraph();
    
    
  }



  openSm(content) {
    this.modalService.open(content, { size: 'sm' });
  }



  /**
 * Total and Gateways Graph
   ------------------------ */
  onTotalNGatewaysTabClick(i?) {
    let series: any[];
    let data: any[];
    let index = i || 0; 

    if (index) {
      series = [this.totalNGateways.graph.series[index]];
      data = [this.totalNGateways.graph.data[index]];
    } else {
      series = this.totalNGateways.graph.series;
      data = this.totalNGateways.graph.data;
    }

    this.totalNGateways.activeTab = index;
    this.totalNGatewaysGraph = {
      series: series,
      data: data
    };
    this.drawChart('line',
    this.totalNGatewaysGraph.data,
    this.totalNGateways.graph.labels,
    this.totalNGatewaysGraph.series
  ); 

 
  }

  getTotalNGatewaysGraph(period?) {
    this.totalNGateways.period = period;

    let params = this.statisticsService.buildParamsFromPeriod(period);

    this.statisticsService.getGeneralTotalNGatewaysValueGraph(this.id, params).subscribe(
      response => {
        this.totalNGateways.graph = response;
        this.onTotalNGatewaysTabClick();
      });
  }


  /**
   * Transaction and Sales Graph
     --------------------------- */
  getTransactionsNSalesGraph(period?) {
    this.transactionsNSales.period = period;

    let params = this.statisticsService.buildParamsFromPeriod(period);
    this.statisticsService.getApplicationGeneralTransactionsNSalesCountGraph(this.id, params).subscribe(
      (response: any) => {
        this.transactionsNSales.graph = response;

        this.drawChart('bar',
        this.transactionsNSales.graph.data, 
        this.transactionsNSales.graph.labels,
        this.transactionsNSales.graph.series
      );   
      });
  }


  getTotalValues(id){
    this.statisticsService.getApplicationGeneralTotalValues(id).subscribe(
      (response:any) =>  {
        this.applicationTotals = response;  
      });
  }

  getApplication(id){
    this.applicationsService.getApplication(id).subscribe(
      (response:any) => {
        this.application = response;
        // this.applicationData.emit(this.application.name);
        // console.log(this.application);
        this.privateService.setIdApplications(this.application);
      });
  }



  drawChart(type:string,arrayObject:any,labels:any, series:any){
    let dataApplications:any[]=[];    
    arrayObject.forEach((element, index) => {      
      dataApplications.push({
           data: element, 
          label: series[index],
          backgroundColor: 'rgba(148,159,177,0.2)'
        })
    });
    
    this.chart = new Chart(type, {
      type: type,
      data: {
        labels: labels,
        datasets: dataApplications
      },
      options: {
        legend: {
          display: false
        },
        scales: {
          xAxes: [{
            display: true
          }],
          yAxes: [{
            display: true
          }],
        }
      }
    });
  }

}


import { Component, OnInit, Input } from '@angular/core';
import { AuthenticationService } from 'src/app/services/authentication.service';
import { ApplicationsService } from 'src/app/services/applications/applications.service';
import { ApplicationsStatisticsService } from 'src/app/services/applications/applications-statistics.service';
import {Chart} from 'chart.js';
import { element } from 'protractor';
import { Router } from '@angular/router';


@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.css']
})
export class ListComponent implements OnInit {
  applicationsList: any;
  totalNApplications: any;
  transactionsNSales: any;
  totalNApplicationsGraph: any;
  chart:any = []; 
  dataLine:any;
  dataBar:any;
  showSubmenu:boolean;
  

  constructor(private router: Router, private auth: AuthenticationService,
    private applicationsService: ApplicationsService,
    private statisticsService: ApplicationsStatisticsService) {
    this.totalNApplications = { period: 'year', activeTab: 0};
    this.transactionsNSales = { period: 'year' };
    this.showSubmenu = false;

  }

  ngOnInit() {
    if (!this.auth.isAuthenticated()) {
      this.router.navigate(['/login']);
    }
    this.getData();
    this.getTotalNApplicationsGraph();
    this.getTransactionsNSalesGraph();

  }

  getData() {
    this.applicationsService.getApplications().subscribe(
      (data: any) => {
        this.applicationsList = data.results;
      },
      error => console.error("Error ... " + error));
  }


  /**
   * Total and Applications Graph
   ------------------------ */
  onTotalNApplicationsTabClick(index: number = 0) {
    let series:any[] = [];
    let data:any[] = [];

    if (index) {
      series = [this.totalNApplications.graph.series[index]];
      data = [this.totalNApplications.graph.data[index]];
      this.dataLine = this.getDataChart(data,series);
      
    } else {
      series = this.totalNApplications.graph.series;
      data = this.totalNApplications.graph.data;
      this.dataLine = this.getDataChart(data, series);
    }

    this.totalNApplications.activeTab = index;
    this.totalNApplicationsGraph = {
      series: series,
      data: data
    };
    

    //this.dataLine = this.getDataChart(this.totalNApplicationsGraph.data, this.totalNApplicationsGraph.series); 
   
  }

  getTotalNApplicationsGraph(period?) {
    this.totalNApplications.period = period;

    let params = this.statisticsService.buildParamsFromPeriod(period);

    this.statisticsService.getGeneralTotalNApplicationsValueGraph(params).subscribe(
      response => {
        this.totalNApplications.graph = response;
        this.onTotalNApplicationsTabClick();
        
      });
  }

  


  /**
   * Transaction and Sales Graph
     --------------------------- */
  getTransactionsNSalesGraph(period?) {
    this.transactionsNSales.period = period;
    let params = this.statisticsService.buildParamsFromPeriod(period);
    this.statisticsService.getGeneralTransactionsNSalesCountGraph(params).subscribe(
      (response: any) => {
        this.transactionsNSales.graph = response;
        this.dataBar = this.getDataChart(this.transactionsNSales.graph.data, this.transactionsNSales.graph.series);   
      });
  }



  getDataChart(arrayObject:any, series:any){    
    let data:any[] = [];    
    arrayObject.forEach((element, index) => {     
      // console.log(element) 
      data.push({
           data: element, 
          label: series[index],
        })
    });
    return data;
  }


  logout() {
    this.auth.logout();
  }

}

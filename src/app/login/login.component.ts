import { Component, OnInit } from '@angular/core';
import { AuthenticationService } from 'src/app/services/authentication.service';
import { Router } from '@angular/router';
import { FormGroup, FormBuilder, FormControl, Validators, NgForm } from '@angular/forms';
import { Title } from '@angular/platform-browser';
import { element } from 'protractor';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})

export class LoginComponent implements OnInit {

  public errors :any= {};
  public user: any = {};
  public email: string = '';
  //loginForm: FormGroup;



  constructor(private router: Router, private auth: AuthenticationService, title: Title) {
    title.setTitle('Login');

  }

  ngOnInit() {
    if (this.auth.isAuthenticated()) {
      this.router.navigate(['/']);
    }
  }


  public login() {
    this.auth.login(this.user.email, this.user.pwd)
      .subscribe(result => {
        if (result) {
          this.router.navigate(['/']);
          location.reload(true);        
        }

      }, error => {        
        this.handlerError(error);
        //this.router.navigate(['/login']);
      });

  }

  handlerError(e){
    this.errors = {};    
    if(e.error.email){          
      this.errors.email = e.error.email 
    }else if(e.error.password){
      this.errors.email = e.error.password 
    }else if(e.error.non_field_errors){
      this.errors.error= e.error.non_field_errors;

    }else{
      console.log(e.error);
    }
  }
}

import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';



// Rutas
import { APP_ROUTES } from './app.routes';

// Modulos
import { PagesModule } from './pages/pages.module';

// Componentes
import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './login/register.component';
import { ForgotComponent } from './login/forgot.component';
import { FooterComponent } from './shared/footer/footer.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { PagesComponent } from 'src/app/pages/pages.component';
import { SharedModule } from './shared/shared.module';
import { RouterModule } from '@angular/router';
import { AppRoutingModule } from './app-routing.module';
import { PrivateService } from 'src/app/services/applications/private.service';
import { MatDialogModule } from '@angular/material';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ActionsButtonsComponent } from './components/actions-buttons/actions-buttons.component';








@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    RegisterComponent,
    ForgotComponent,
    PagesComponent,

    
    // ActionsComponent,    
    // CreateOrEditComponent,    
  ],
  imports: [
    // APP_ROUTES,
    AppRoutingModule,
    BrowserModule,  
    SharedModule,  
    // PagesModule,
    FormsModule,    
    ReactiveFormsModule,
    HttpClientModule,
    NgbModule.forRoot(),    
    BrowserAnimationsModule,
    MatDialogModule
      
  ],
  exports:[],
  providers: [PrivateService],
  bootstrap: [AppComponent]
})
export class AppModule { }

import { Injectable } from '@angular/core';
import { GenericService } from 'src/app/services/generic.service';

@Injectable({
  providedIn: 'root'
})
export class CurrenciesService {

  constructor(private service: GenericService) { }



    /**
     * @name getCurrencies
     * @desc Queries getCurrencies from server
     * @param params (optional)
     * @returns the promise of getCurrencies
     */
    getCurrencies (params:any={}):any {
      // params = params || {};
      return this.service.getAllCustom('currencies/');
    }

    /**
     * @name getCurrency
     * @desc Queries getCurrency from server
     * @param pk
     * @param params (optional)
     * @returns the promise of getCurrency
     */
    getCurrency (pk, params:any={}) {
      // params = params || {};
      return this.service.getOne('currencies/',pk);
    }
}

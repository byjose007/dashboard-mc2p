import { Injectable } from '@angular/core';
import { isDefined } from '@angular/compiler/src/util';
import { Router } from '@angular/router';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { catchError, tap, map } from 'rxjs/operators';
import { Constants } from 'src/app/constants';


@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {

  private api_url = Constants.settings.API.BASE_URL;
  private httpOptions = {};


  constructor(private http: HttpClient, private router: Router) { }


  getAuthToken() {
    return localStorage.getItem('MC2PAuthToken');
  }

  setAuthToken(_token) {
    let existingToken = this.getAuthToken();
    let token = _token || existingToken;
    localStorage.setItem('MC2PAuthToken', token);
    //setBaseUrl
  }

  delAuthToken() {
    localStorage.removeItem('MC2PAuthToken');
  }

  isAuthenticated() {
    return this.getAuthToken() != null ? true : false
    
  }

  addAuthorization(token) {

    if (isDefined(token)) {
      this.setAuthToken(token);
    }

    if (this.isAuthenticated()) {
      let authorization = 'Token ' + this.getAuthToken();
      this.httpOptions = {
        headers: new HttpHeaders({
          'Content-Type': 'application/json',
          'Authorization': authorization
        })
      };
    }
  }

  removeAuthorization() {
    this.delAuthToken();
    //this.restangular.setDefaultHeaders({ });
  }

  register(username, password1, password2, email, more): Observable<any> {
    let data = {
      "username": username,
      "password1": password1,
      "password2": password2,
      "email": email
    };


    return this.http.post<any>(this.api_url + 'auth/registration/', data, this.httpOptions).pipe(
      map((data: any) => {
        if (isDefined(data.key)) {
          this.addAuthorization(data.key)
        }

      })
    );
  }

  verify(key): Observable<any> {
    let data = { 'key': key };
    return this.http.post<any>(this.api_url + 'auth/registration/verify-email/', data, this.httpOptions);

  }

  login(email, password): Observable<any> {
    let data = {
      "email": email,
      "password": password
    };

    return this.http.post<any>(this.api_url + 'auth/login/', data, this.httpOptions).pipe(
        tap((data:any)=>{
          this.addAuthorization(data.key)          
      
        })           
      
    );
  }


  logout(): Observable<any> {
    let data = {};
    this.removeAuthorization();
    return this.http.post<any>(this.api_url + 'auth/logout/', data, this.httpOptions);
  }

  passwordReset(email): Observable<any> {
    let data = { email: email };
    return this.http.post<any>(this.api_url + 'auth/password/reset/', data, this.httpOptions);


  }

  confirmPasswordReset(uid, token, newPassword1, newPassword2): Observable<any> {
    let data = {
      'uid': uid,
      'token': token,
      'new_password1': newPassword1,
      'new_password2': newPassword2
    };
    return this.http.post<any>(this.api_url + 'auth/password/reset/confirm/', data, this.httpOptions);
  }

  getUser() {
    return this.http.get<any>(this.api_url + 'auth/user/', this.httpOptions);
  }

  /**
   * @name updateUser
   * @desc Queries all weddings
   * @param {hash} params
   * Possible values:
   *   params.username - optional
   *   params.first_name - optional
   *   params.last_name - optional
   *   params.email - forbidden. Email is used to identify the user, so it can't be changed
   * @returns the promise of updatedUser
   */

  TODO
  /* updateUser (params) {
    let requestParams = {};
    let data =  [{
      'username': 'username',
      'first_name': 'firstName',
      'last_name': 'lastName'
    }];
    data.forEach((paramsName, requestName) => {
      if (isDefined(params[paramsName])) {
        requestParams[requestName] = params[paramsName];
      }
    });

    return this.restangular.one('auth/user').patch(requestParams);
  }*/

  updateUserPassword(oldPassword, newPassword1, newPassword2): Observable<any> {
    let data = {
      'old_password': oldPassword,
      'new_password1': newPassword1,
      'new_password2': newPassword2
    };

    return this.http.post<any>(this.api_url + 'auth/password/change/', data, this.httpOptions);
  }
}

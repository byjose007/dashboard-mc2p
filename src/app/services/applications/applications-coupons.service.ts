import { Injectable } from '@angular/core';
import { isUndefined } from 'util';
import { GenericService } from 'src/app/services/generic.service';

@Injectable({
  providedIn: 'root'
})
export class ApplicationsCouponsService {

  constructor(private service: GenericService) { }

  read(applicationId, id) {
    return this.getApplicationCoupon(applicationId, id);
  }

  list(applicationId, params: any = {}) {
    return this.getApplicationCoupons(applicationId, params);
  }

  save(applicationId, couponData) {
    return this.saveApplicationCoupon(applicationId, couponData);
  }

  delete(applicationId, id){
    return this.deleteApplicationCoupon(applicationId,id);
  }



    /**
     * @name getCouponTypeChoices
     * @desc Queries getCouponTypeChoices from server
     * @returns the promise of getCouponTypeChoices
     */
    getCouponTypeChoices () {
      return [
        {name: 'Percent', id: 'P'},
        {name: 'Amount', id: 'A'},
      ];
    }

    /**
     * @name _buildFormData
     * @desc build FormData object to send on the request to the server
     * @param params (optional)
     * @returns the object of FormData
     */
    _buildFormData (params) {
      let fd = new FormData();

      params.forEach( (value, key) => {
        if (key === 'coupon_type' && value.id) {
          fd.append(key, value.id);
        }
        else if (value) {
          fd.append(key, value);
        }
      });

      return fd;
    }

    /**
     * @name createApplicationCoupon
     * @desc Queries createApplicationCoupon from server
     * @param applicationId
     * @param params (optional)
     * @returns the promise of createApplicationCoupon
     */
    createApplicationCoupon (applicationId, params: any = {}) {
      // let fd = this._buildFormData(params);
      return this.service.create(`applications/${applicationId}/coupons/`, params);
    }

    /**
     * @name getApplicationCoupons
     * @desc Queries getApplicationCoupons from server
     * @param applicationId
     * @param params (optional)
     * @returns the promise of getApplicationCoupons
     */
    getApplicationCoupons (applicationId, params: any = {}) {
      return this.service.getAllCustom(`applications/${applicationId}/coupons/`, params);
    }

    /**
     * @name getApplicationCoupon
     * @desc Queries getApplicationCoupon from server
     * @param applicationId
     * @param id
     * @returns the promise of getApplicationCoupon
     */
    getApplicationCoupon (applicationId, id) {
      return this.service.getOne(`applications/${applicationId}/coupons/`, id);
    }

    /**
     * @name updateApplicationCoupon
     * @desc Queries updateApplicationCoupon from server
     * @param applicationId
     * @param id
     * @param params (optional)
     * @returns the promise of updateApplicationCoupon
     */
    updateApplicationCoupon (applicationId, id, params: any = {}) {
      // let fd = this._buildFormData(params);
      return this.service.update(`applications/${applicationId}/coupons/`, id, params);
    }

    /**
     * @name cloneApplicationCoupon
     * @desc Queries cloneApplicationCoupon from server
     * @param applicationId
     * @param id
     * @returns the promise of cloneApplicationCoupon
     */
    cloneApplicationCoupon (applicationId, id) {
      return this.service.create(`applications/${applicationId}/coupons/${id}/clone/`);
    }

    /**
     * @name deleteApplicationCoupon
     * @desc Queries deleteApplicationCoupon from server
     * @param applicationId
     * @param id
     * @returns the promise of deleteApplicationCoupon
     */
    deleteApplicationCoupon (applicationId, id) {
      return this.service.remove(`applications/${applicationId}/coupons/`, id);
    }

    /**
     * @name saveApplicationCoupon
     * @desc Queries saveApplicationCoupon from server
     * @param applicationId
     * @param params (optional)
     * @returns the promise of saveApplicationCoupon
     */
    saveApplicationCoupon (applicationId, couponData) {
      let couponId = couponData.id;

      if (couponId == null) {
        return this.createApplicationCoupon(applicationId, couponData);
      } else {
        return this.updateApplicationCoupon(applicationId, couponId, couponData);
      }
    }

}

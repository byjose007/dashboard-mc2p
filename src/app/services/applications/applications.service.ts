import { Injectable } from '@angular/core';
import { isDefined } from '@angular/compiler/src/util';
import { isString, isUndefined } from 'util';
import { Observable } from 'rxjs';
import { GenericService } from 'src/app/services/generic.service';




@Injectable({
  providedIn: 'root'
})
export class ApplicationsService {

  public fb: any = {};
  aplications: any = {};

  constructor(private service: GenericService) { }




  /**
    * @name getOperationTypeChoices
    * @desc Queries getOperationTypeChoices from server
    * @returns the promise of getOperationTypeChoices
    */
  getOperationTypeChoices() {
    return [
      { name: 'Payment', id: 'P' },
      { name: 'Suscripción', id: 'S' },
      { name: 'Autorización', id: 'A' },
    ];
  }



  /**
   * @name _buildFormData
   * @desc build FormData object to send on the request to the server
   * @param params (optional)
   * @returns the object of FormData
   */
  _buildFormData(params) {
    let fd = new FormData();

    if (isDefined(params.name)) {
      fd.append('name', params.name);
    }
    if (params.logo !== null && isDefined(params.logo) && (!isString(params.logo) || !params.logo.startsWith('http'))) {
      fd.append('logo', params.logo);
    }
    if (isDefined(params.test)) {
      fd.append('test', params.test);
    }
    if (params.url !== null && isDefined(params.url)) {
      fd.append('url', params.url);
    }

    return fd;
  }

  /**
  * @name createApplication
  * @desc Queries createApplication from server
  * @param params (optional)
  * @returns the promise of createApplication
  */
  createApplication(params: any = {}) {
    // let fd = this._buildFormData(params);
    return this.service.create('applications/', params);
  }


  /**
 * @name getApplications
 * @desc Queries getApplications from server
 * @param params (optional)
 * @returns the promise of getApplications
 */
  getApplications(params: any = {}) {
    return this.service.getAll('applications/');
  }



  /**
   * @name getApplication
   * @desc Queries getApplication from server
   * @param applicationId
   * @returns the promise of getApplication
   */
  getApplication(applicationId) {
    return this.service.getOne('applications/', applicationId);

  }

  /**
   * @name updateApplication
   * @desc Queries updateApplication from server
   * @param applicationId
   * @param params (optional)
   * @returns the promise of updateApplication
   */
  updateApplication(applicationId, params: any = {}) {
    // let fd = this._buildFormData(params);
    return this.service.update('applications/', applicationId, params)
  }

  /**
 * @name saveApplication
 * @desc Queries saveApplication from server
 * @param applicationId
 * @param params (optional)
 * @returns the promise of saveApplication
 */
  saveApplication(applicationData) {
    let appId = applicationData.id;
    if (appId==null) {
      return this.createApplication(applicationData);
    } else {
      return this.updateApplication(appId, applicationData);
    }
  }


  /**
   * @name deleteApplication
   * @desc Queries deleteApplication from server
   * @param applicationId
   * @returns the promise of deleteApplication
   */
  deleteApplication(applicationId) {
    return this.service.remove('applications/', applicationId);
  }



  /**
   * @name getApplicationKeys
   * @desc Queries getApplicationKeys from server
   * @param applicationId
   * @returns the promise of getApplicationKeys
   */
  getApplicationKeys(applicationId) {
    return this.service.getCustom('applications/', applicationId, 'keys/');
  }

  /**
    * @name getApplicationPayments
    * @desc Queries getApplicationPayments from server
    * @param applicationId
    * @param params (optional)
    * @returns the promise of getApplicationPayments
    */
  getApplicationPayments(applicationId, params: any = {}) {
    return this.service.getAllCustom(`applications/${applicationId}/payments/`, params);
  }


  /**
   * @name getApplicationPayment
   * @desc Queries getApplicationPayment from server
   * @param applicationId
   * @param id
   * @returns the promise of getApplicationPayment
   */
  getApplicationPayment(applicationId, id) {
    return this.service.getCustom('applications/', applicationId, 'payments/', id);
  }


  /**
   * @name getApplicationTransactions
   * @desc Queries getApplicationTransactions from server
   * @param applicationId
   * @param params (optional)
   * @returns the promise of getApplicationTransactions
   */
  getApplicationTransactions(applicationId, params: any = {}) {
    return this.service.getCustom('applications/', applicationId, 'transactions/');
  }


  /**
   * @name getApplicationTransaction
   * @desc Queries getApplicationTransaction from server
   * @param applicationId
   * @param id
   * @returns the promise of getApplicationTransaction
   */
  getApplicationTransaction(applicationId, id) {
    return this.service.getCustom('applications/', applicationId, 'transactions/', id);
  }

}


import { Injectable } from '@angular/core';
import * as Moment from 'moment';
import { isDefined } from '@angular/compiler/src/util';
import { isString, isUndefined } from 'util';
import { GenericService } from 'src/app/services/generic.service';
import { isObject } from 'lodash';

@Injectable({
  providedIn: 'root'
})
export class ApplicationsStatisticsService {

  constructor(private service: GenericService) { }

  /***********
   * GENERAL *
   ***********/

  /**
   * @name buildParamsFromPeriod
   * @desc Build the Restangular endpoint
   * @param applicationId (optional)
   * @returns the restangular instace of buildParamsFromPeriod
   */
  buildParamsFromPeriod(period) {
    let params: any;

    if (period === 'week') {
      params = {
        'period': 'day',
        'min_date': Moment().subtract(1, 'week').format('MM/D/YYYY')
      };
    } else if (period === 'month') {
      params = {
        'min_date': Moment().subtract(1, 'month').format('MM/D/YYYY')
      };
    } else {
      params = {
        'period': 'month',
        'min_date': Moment().subtract(1, 'year').format('MM/D/YYYY')
      };
    }

    return params;
  }

  /**
   * @name _buildGeneralStatsEndpoint
   * @desc Build the Restangular endpoint
   * @param applicationId (optional)
   * @returns the restangular instace of _buildGeneralStatsEndpoint
   */
  _buildGeneralStatsEndpoint(applicationId) {
    if (isDefined(applicationId) && isString(applicationId)) {
      return this.service.getOne('applications/', applicationId);
    }
  }



  /**
   * @name getGeneralTotalNApplicationsValueGraph
   * @desc Queries getGeneralTotalNApplicationsValueGraph from server
   * @param applicationId (optional)
   * @param params (optional)
   * @returns the promise of getGeneralTotalNApplicationsValueGraph
   */
  getGeneralTotalNApplicationsValueGraph(applicationId?, params?) {
    if (isObject(applicationId) && isUndefined(params)) {
      params = applicationId;
    }
    this._buildGeneralStatsEndpoint(applicationId);
    return this.service.getAllCustom('stats/general/total-and-applications-value-graph/', params);

  }

  /**
   * @name getGeneralTotalNGatewaysValueGraph
   * @desc Queries getGeneralTotalNGatewaysValueGraph from server
   * @param applicationId (optional)
   * @param params (optional)
   * @returns the promise of getGeneralTotalNGatewaysValueGraph
   */
  getGeneralTotalNGatewaysValueGraph(applicationId?, params: any = {}) {
    if (isObject(applicationId) && isUndefined(params)) {
      params = applicationId;
    }
    this._buildGeneralStatsEndpoint(applicationId);
    return this.service.getAllCustom(`applications/${applicationId}/stats/general/total-and-gateways-value-graph/`, params);

  }


  /**
   * @name getGeneralTransactionsNSalesCountGraph
   * @desc Queries getGeneralTransactionsNSalesCountGraph from server
   * @param applicationId (optional)
   * @returns the promise of getGeneralTransactionsNSalesCountGraph
   */

  getGeneralTransactionsNSalesCountGraph(applicationId?, params: any = {}) {
    if (isObject(applicationId) && isUndefined(params)) {
      params = applicationId;
    }
    this._buildGeneralStatsEndpoint(params);
    return this.service.getAllCustom('stats/general/transactions-and-sales-count-graph/', params);
  }

  /**
 * @name getGeneralTotalValues
 * @desc Queries getGeneralTotalValues from server
 * @param applicationId (optional)
 * @param params (optional)
 * @returns the promise of getGeneralTotalValues
 */
  getGeneralTotalValues(applicationId?, params: any = {}) {
    if (isObject(applicationId) && isUndefined(params)) {
      params = applicationId;
    }
    // params = params || {};

    var _restangular = this._buildGeneralStatsEndpoint(params);

    return this.service.getAllCustom('stats/general/total_values/', params);

  }


  /*********
   * APPLICATION GENERAL *
   *********/

  /**
   * @name getApplicationGeneralTransactionsNSalesCountGraph
   * @desc Queries getApplicationGeneralTransactionsNSalesCountGraph from server
   * @param applicationId
   * @param params (optional)
   * @returns the promise of getApplicationGeneralTransactionsNSalesCountGraph
   */
  getApplicationGeneralTransactionsNSalesCountGraph(applicationId, params: any = {}) {
    return this.service.getAllCustom(`applications/${applicationId}/stats/general/transactions-and-sales-count-graph/`, params);
  }

  /**
   * @name getApplicationGeneralTotalValues
   * @desc Queries getApplicationGeneralTotalValues from server
   * @param applicationId
   * @param params (optional)
   * @returns the promise of getApplicationGeneralTotalValues
   */
  getApplicationGeneralTotalValues(applicationId, params: any = {}) {
    return this.service.getAllCustom(`applications/${applicationId}/stats/general/total_values/`, params);
  }



  /*********
   * SALES *
   *********/
/**
 * @name getApplicationSalesCountGraph
 * @desc Queries getApplicationSalesCountGraph from server
 * @param applicationId
 * @param params (optional)
 * @returns the promise of getApplicationSalesCountGraph
 */
getApplicationSalesCountGraph(applicationId, params: any = {}) {
  return this.service.getAllCustom(`applications/${applicationId}/stats/sales/count-graph/`, params);
}

  /**
  * @name getApplicationSalesValueGraph
  * @desc Queries getApplicationSalesValueGraph from server
  * @param applicationId
  * @param params (optional)
  * @returns the promise of getApplicationSalesValueGraph
  */
  getApplicationSalesValueGraph(applicationId, params: any = {}) {
    return this.service.getAllCustom(`applications/${applicationId}/stats/sales/value-graph/`, params);
  }


}







// /***********
//    * GENERAL *
//    ***********/

// /**
//  * @name buildParamsFromPeriod
//  * @desc Build the Restangular endpoint
//  * @param applicationId (optional)
//  * @returns the restangular instace of buildParamsFromPeriod
//  */
// function buildParamsFromPeriod(period) {
//   var params;

//   if (period === 'week') {
//     params = {
//       'period': 'day',
//       'min_date': Moment().subtract(1, 'week').format('MM/D/YYYY')
//     };
//   } else if (period === 'month') {
//     params = {
//       'min_date': Moment().subtract(1, 'month').format('MM/D/YYYY')
//     };
//   } else {
//     params = {
//       'period': 'month',
//       'min_date': Moment().subtract(1, 'year').format('MM/D/YYYY')
//     };
//   }

//   return params;
// }

// /**
//  * @name _buildGeneralStatsEndpoint
//  * @desc Build the Restangular endpoint
//  * @param applicationId (optional)
//  * @returns the restangular instace of _buildGeneralStatsEndpoint
//  */
// function _buildGeneralStatsEndpoint(applicationId) {
//   var _restangular = Restangular;

//   if (angular.isDefined(applicationId) && angular.isString(applicationId)) {
//     _restangular = _restangular.one('applications', applicationId);
//   }

//   return _restangular;
// }

// /**
//  * @name getGeneralTotalNApplicationsValueGraph
//  * @desc Queries getGeneralTotalNApplicationsValueGraph from server
//  * @param applicationId (optional)
//  * @param params (optional)
//  * @returns the promise of getGeneralTotalNApplicationsValueGraph
//  */
// function getGeneralTotalNApplicationsValueGraph(applicationId, params) {
//   if (angular.isObject(applicationId) && angular.isUndefined(params)) {
//     params = applicationId;
//   }
//   params = params || {};

//   var _restangular = _buildGeneralStatsEndpoint(applicationId);

//   return _restangular
//     .all('stats/general/total-and-applications-value-graph')
//     .customGET('', params);
// }

// /**
//  * @name getGeneralTotalNGatewaysValueGraph
//  * @desc Queries getGeneralTotalNGatewaysValueGraph from server
//  * @param applicationId (optional)
//  * @param params (optional)
//  * @returns the promise of getGeneralTotalNGatewaysValueGraph
//  */
// function getGeneralTotalNGatewaysValueGraph(applicationId, params) {
//   if (angular.isObject(applicationId) && angular.isUndefined(params)) {
//     params = applicationId;
//   }
//   params = params || {};

//   var _restangular = _buildGeneralStatsEndpoint(applicationId);

//   return _restangular
//     .all('stats/general/total-and-gateways-value-graph')
//     .customGET('', params);
// }

// /**
//  * @name getGeneralTransactionsNSalesCountGraph
//  * @desc Queries getGeneralTransactionsNSalesCountGraph from server
//  * @param applicationId (optional)
//  * @returns the promise of getGeneralTransactionsNSalesCountGraph
//  */
// function getGeneralTransactionsNSalesCountGraph(applicationId, params) {
//   if (angular.isObject(applicationId) && angular.isUndefined(params)) {
//     params = applicationId;
//   }
//   params = params || {};

//   var _restangular = _buildGeneralStatsEndpoint(params);

//   return _restangular
//     .all('stats/general/transactions-and-sales-count-graph')
//     .customGET('', params);
// }

// /**
//  * @name getGeneralTotalValues
//  * @desc Queries getGeneralTotalValues from server
//  * @param applicationId (optional)
//  * @param params (optional)
//  * @returns the promise of getGeneralTotalValues
//  */
// function getGeneralTotalValues(applicationId, params) {
//   if (angular.isObject(applicationId) && angular.isUndefined(params)) {
//     params = applicationId;
//   }
//   params = params || {};

//   var _restangular = _buildGeneralStatsEndpoint(params);

//   return _restangular
//     .all('stats/general/total_values')
//     .customGET('', params);
// }


// /*********
//  * APPLICATION GENERAL *
//  *********/

// /**
//  * @name getApplicationGeneralTransactionsNSalesCountGraph
//  * @desc Queries getApplicationGeneralTransactionsNSalesCountGraph from server
//  * @param applicationId
//  * @param params (optional)
//  * @returns the promise of getApplicationGeneralTransactionsNSalesCountGraph
//  */
// function getApplicationGeneralTransactionsNSalesCountGraph(applicationId, params) {
//   params = params || {};
//   return Restangular
//     .one('applications', applicationId)
//     .all('stats/general/transactions-and-sales-count-graph')
//     .customGET('', params);
// }

// /**
//  * @name getApplicationGeneralTotalValues
//  * @desc Queries getApplicationGeneralTotalValues from server
//  * @param applicationId
//  * @param params (optional)
//  * @returns the promise of getApplicationGeneralTotalValues
//  */
// function getApplicationGeneralTotalValues(applicationId, params) {
//   params = params || {};
//   return Restangular
//     .one('applications', applicationId)
//     .all('stats/general/total_values')
//     .customGET('', params);
// }


// /*********
//  * SALES *
//  *********/

// /**
//  * @name getApplicationSalesCountAvg
//  * @desc Queries getApplicationSalesCountAvg from server
//  * @param applicationId
//  * @returns the promise of getApplicationSalesCountAvg
//  */
// function getApplicationSalesCountAvg(applicationId) {
//   return Restangular
//     .one('applications', applicationId)
//     .all('stats/sales/count-avg')
//     .get();
// }

// /**
//  * @name getApplicationSalesCountGraph
//  * @desc Queries getApplicationSalesCountGraph from server
//  * @param applicationId
//  * @param params (optional)
//  * @returns the promise of getApplicationSalesCountGraph
//  */
// function getApplicationSalesCountGraph(applicationId, params) {
//   params = params || {};

//   return Restangular
//     .one('applications', applicationId)
//     .all('stats/sales/count-graph')
//     .customGET('', params);
// }

// /**
//  * @name getApplicationSalesCount
//  * @desc Queries getApplicationSalesCount from server
//  * @param applicationId
//  * @returns the promise of getApplicationSalesCount
//  */
// function getApplicationSalesCount(applicationId) {
//   return Restangular
//     .one('applications', applicationId)
//     .all('stats/sales/count')
//     .get();
// }
// /**
//  * @name getApplicationSalesValueAvg
//  * @desc Queries getApplicationSalesValueAvg from server
//  * @param applicationId
//  * @returns the promise of getApplicationSalesValueAvg
//  */
// function getApplicationSalesValueAvg(applicationId) {
//   return Restangular
//     .one('applications', applicationId)
//     .all('stats/sales/value-avg')
//     .get();
// }

// /**
//  * @name getApplicationSalesValueGraph
//  * @desc Queries getApplicationSalesValueGraph from server
//  * @param applicationId
//  * @param params (optional)
//  * @returns the promise of getApplicationSalesValueGraph
//  */
// function getApplicationSalesValueGraph(applicationId, params) {
//   params = params || {};

//   return Restangular
//     .one('applications', applicationId)
//     .all('stats/sales/value-graph')
//     .customGET('', params);
// }

// /**
//  * @name getApplicationSalesValue
//  * @desc Queries getApplicationSalesValue from server
//  * @param applicationId
//  * @returns the promise of getApplicationSalesValue
//  */
// function getApplicationSalesValue(applicationId) {
//   return Restangular
//     .one('applications', applicationId)
//     .all('stats/sales/value')
//     .get();
// }


// /*****************
//  * PRODUCT SALES *
//  *****************/

// /**
//  * @name getApplicationProductSalesCountAvg
//  * @desc Queries getApplicationProductSalesCountAvg from server
//  * @param applicationId
//  * @returns the promise of getApplicationProductSalesCountAvg
//  */
// function getApplicationProductSalesCountAvg(applicationId) {
//   return Restangular
//     .one('applications', applicationId)
//     .all('stats/one-payment-sales/count-avg')
//     .get();
// }

// /**
//  * @name getApplicationProductSalesCountGraph
//  * @desc Queries getApplicationProductSalesCountGraph from server
//  * @param applicationId
//  * @param params (optional)
//  * @returns the promise of getApplicationProductSalesCountGraph
//  */
// function getApplicationProductSalesCountGraph(applicationId, params) {
//   params = params || {};

//   return Restangular
//     .one('applications', applicationId)
//     .all('stats/one-payment-sales/count-graph')
//     .customGET('', params);
// }

// /**
//  * @name getApplicationProductSalesCount
//  * @desc Queries getApplicationProductSalesCount from server
//  * @param applicationId
//  * @returns the promise of getApplicationProductSalesCount
//  */
// function getApplicationProductSalesCount(applicationId) {
//   return Restangular
//     .one('applications', applicationId)
//     .all('stats/one-payment-sales/count')
//     .get();
// }

// /**
//  * @name getApplicationProductSalesValueAvg
//  * @desc Queries getApplicationProductSalesValueAvg from server
//  * @param applicationId
//  * @returns the promise of getApplicationProductSalesValueAvg
//  */
// function getApplicationProductSalesValueAvg(applicationId) {
//   return Restangular
//     .one('applications', applicationId)
//     .all('stats/one-payment-sales/value-avg')
//     .get();
// }

// /**
//  * @name getApplicationProductSalesValueGraph
//  * @desc Queries getApplicationProductSalesValueGraph from server
//  * @param applicationId
//  * @param params (optional)
//  * @returns the promise of getApplicationProductSalesValueGraph
//  */
// function getApplicationProductSalesValueGraph(applicationId, params) {
//   params = params || {};

//   return Restangular
//     .one('applications', applicationId)
//     .all('stats/one-payment-sales/value-graph')
//     .customGET('', params);
// }

// /**
//  * @name getApplicationProductSalesValue
//  * @desc Queries getApplicationProductSalesValue from server
//  * @param applicationId
//  * @returns the promise of getApplicationProductSalesValue
//  */
// function getApplicationProductSalesValue(applicationId) {
//   return Restangular
//     .one('applications', applicationId)
//     .all('stats/one-payment-sales/value')
//     .get();
// }


// /**********************
//  * SUBSCRIPTION SALES *
//  **********************/

// /**
//  * @name getApplicationSubscriptionSalesCountAvg
//  * @desc Queries getApplicationSubscriptionSalesCountAvg from server
//  * @param applicationId
//  * @returns the promise of getApplicationSubscriptionSalesCountAvg
//  */
// function getApplicationSubscriptionSalesCountAvg(applicationId) {
//   return Restangular
//     .one('applications', applicationId)
//     .all('stats/subscription-sales/count-avg')
//     .get();
// }

// /**
//  * @name getApplicationSubscriptionSalesCountGraph
//  * @desc Queries getApplicationSubscriptionSalesCountGraph from server
//  * @param applicationId
//  * @param params (optional)
//  * @returns the promise of getApplicationSubscriptionSalesCountGraph
//  */
// function getApplicationSubscriptionSalesCountGraph(applicationId, params) {
//   params = params || {};

//   return Restangular
//     .one('applications', applicationId)
//     .all('stats/subscription-sales/count-graph')
//     .customGET('', params);
// }

// /**
//  * @name getApplicationSubscriptionSalesCount
//  * @desc Queries getApplicationSubscriptionSalesCount from server
//  * @param applicationId
//  * @returns the promise of getApplicationSubscriptionSalesCount
//  */
// function getApplicationSubscriptionSalesCount(applicationId) {
//   return Restangular
//     .one('applications', applicationId)
//     .all('stats/subscription-sales/count')
//     .get();
// }

// /**
//  * @name getApplicationSubscriptionSalesValueAvg
//  * @desc Queries getApplicationSubscriptionSalesValueAvg from server
//  * @param applicationId
//  * @returns the promise of getApplicationSubscriptionSalesValueAvg
//  */
// function getApplicationSubscriptionSalesValueAvg(applicationId) {
//   return Restangular
//     .one('applications', applicationId)
//     .all('stats/subscription-sales/value-avg')
//     .get();
// }

// /**
//  * @name getApplicationSubscriptionSalesValueGraph
//  * @desc Queries getApplicationSubscriptionSalesValueGraph from server
//  * @param applicationId
//  * @param params (optional)
//  * @returns the promise of getApplicationSubscriptionSalesValueGraph
//  */
// function getApplicationSubscriptionSalesValueGraph(applicationId, params) {
//   params = params || {};

//   return Restangular
//     .one('applications', applicationId)
//     .all('stats/subscription-sales/value-graph')
//     .customGET('', params);
// }

// /**
//  * @name getApplicationSubscriptionSalesValue
//  * @desc Queries getApplicationSubscriptionSalesValue from server
//  * @param applicationId
//  * @returns the promise of getApplicationSubscriptionSalesValue
//  */
// function getApplicationSubscriptionSalesValue(applicationId) {
//   return Restangular
//     .one('applications', applicationId)
//     .all('stats/subscription-sales/value')
//     .get();
// }


// /****************
//  * TRANSACTIONS *
//  ****************/

// /**
//  * @name getApplicationTransactionsCountAvg
//  * @desc Queries getApplicationTransactionsCountAvg from server
//  * @param applicationId
//  * @returns the promise of getApplicationTransactionsCountAvg
//  */
// function getApplicationTransactionsCountAvg(applicationId) {
//   return Restangular
//     .one('applications', applicationId)
//     .all('stats/transactions/count-avg')
//     .get();
// }

// /**
//  * @name getApplicationTransactionsCountGraph
//  * @desc Queries getApplicationTransactionsCountGraph from server
//  * @param applicationId
//  * @param params (optional)
//  * @returns the promise of getApplicationTransactionsCountGraph
//  */
// function getApplicationTransactionsCountGraph(applicationId, params) {
//   params = params || {};

//   return Restangular
//     .one('applications', applicationId)
//     .all('stats/transactions/count-graph')
//     .customGET('', params);
// }

// /**
//  * @name getApplicationTransactionsCount
//  * @desc Queries getApplicationTransactionsCount from server
//  * @param applicationId
//  * @returns the promise of getApplicationTransactionsCount
//  */
// function getApplicationTransactionsCount(applicationId) {
//   return Restangular
//     .one('applications', applicationId)
//     .all('stats/transactions/count')
//     .get();
// }

// /**
//  * @name getApplicationTransactionsValueAvg
//  * @desc Queries getApplicationTransactionsValueAvg from server
//  * @param applicationId
//  * @returns the promise of getApplicationTransactionsValueAvg
//  */
// function getApplicationTransactionsValueAvg(applicationId) {
//   return Restangular
//     .one('applications', applicationId)
//     .all('stats/transactions/value-avg')
//     .get();
// }

// /**
//  * @name getApplicationTransactionsValueGraph
//  * @desc Queries getApplicationTransactionsValueGraph from server
//  * @param applicationId
//  * @param params (optional)
//  * @returns the promise of getApplicationTransactionsValueGraph
//  */
// function getApplicationTransactionsValueGraph(applicationId, params) {
//   params = params || {};

//   return Restangular
//     .one('applications', applicationId)
//     .all('stats/transactions/value-graph')
//     .customGET('', params);
// }

// /**
//  * @name getApplicationTransactionsValue
//  * @desc Queries getApplicationTransactionsValue from server
//  * @param applicationId
//  * @returns the promise of getApplicationTransactionsValue
//  */
// function getApplicationTransactionsValue(applicationId) {
//   return Restangular
//     .one('applications', applicationId)
//     .all('stats/transactions/value')
//     .get();
// }


// /****************************
//  * ONE PAYMENT TRANSACTIONS *
//  ****************************/

// /**
//  * @name getApplicationOnePaymentTransactionsCountAvg
//  * @desc Queries getApplicationOnePaymentTransactionsCountAvg from server
//  * @param applicationId
//  * @returns the promise of getApplicationOnePaymentTransactionsCountAvg
//  */
// function getApplicationOnePaymentTransactionsCountAvg(applicationId) {
//   return Restangular
//     .one('applications', applicationId)
//     .all('stats/one-payment-transactions/count-avg')
//     .get();
// }

// /**
//  * @name getApplicationOnePaymentTransactionsCountGraph
//  * @desc Queries getApplicationOnePaymentTransactionsCountGraph from server
//  * @param applicationId
//  * @param params (optional)
//  * @returns the promise of getApplicationOnePaymentTransactionsCountGraph
//  */
// function getApplicationOnePaymentTransactionsCountGraph(applicationId, params) {
//   params = params || {};

//   return Restangular
//     .one('applications', applicationId)
//     .all('stats/one-payment-transactions/count-graph')
//     .customGET('', params);
// }

// /**
//  * @name getApplicationOnePaymentTransactionsCount
//  * @desc Queries getApplicationOnePaymentTransactionsCount from server
//  * @param applicationId
//  * @returns the promise of getApplicationOnePaymentTransactionsCount
//  */
// function getApplicationOnePaymentTransactionsCount(applicationId) {
//   return Restangular
//     .one('applications', applicationId)
//     .all('stats/one-payment-transactions/count')
//     .get();
// }

// /**
//  * @name getApplicationOnePaymentTransactionsValueAvg
//  * @desc Queries getApplicationOnePaymentTransactionsValueAvg from server
//  * @param applicationId
//  * @returns the promise of getApplicationOnePaymentTransactionsValueAvg
//  */
// function getApplicationOnePaymentTransactionsValueAvg(applicationId) {
//   return Restangular
//     .one('applications', applicationId)
//     .all('stats/one-payment-transactions/value-avg')
//     .get();
// }

// /**
//  * @name getApplicationOnePaymentTransactionsValueGraph
//  * @desc Queries getApplicationOnePaymentTransactionsValueGraph from server
//  * @param applicationId
//  * @param params (optional)
//  * @returns the promise of getApplicationOnePaymentTransactionsValueGraph
//  */
// function getApplicationOnePaymentTransactionsValueGraph(applicationId, params) {
//   params = params || {};

//   return Restangular
//     .one('applications', applicationId)
//     .all('stats/one-payment-transactions/value-graph')
//     .customGET('', params);
// }

// /**
//  * @name getApplicationOnePaymentTransactionsValue
//  * @desc Queries getApplicationOnePaymentTransactionsValue from server
//  * @param applicationId
//  * @returns the promise of getApplicationOnePaymentTransactionsValue
//  */
// function getApplicationOnePaymentTransactionsValue(applicationId) {
//   return Restangular
//     .one('applications', applicationId)
//     .all('stats/one-payment-transactions/value')
//     .get();
// }


// /*****************************
//  * SUBSCRIPTION TRANSACTIONS *
//  *****************************/

// /**
//  * @name getApplicationSubscriptionTransactionsCountAvg
//  * @desc Queries getApplicationSubscriptionTransactionsCountAvg from server
//  * @param applicationId
//  * @returns the promise of getApplicationSubscriptionTransactionsCountAvg
//  */
// function getApplicationSubscriptionTransactionsCountAvg(applicationId) {
//   return Restangular
//     .one('applications', applicationId)
//     .all('stats/subscription-transactions/count-avg')
//     .get();
// }

// /**
//  * @name getApplicationSubscriptionTransactionsCountGraph
//  * @desc Queries getApplicationSubscriptionTransactionsCountGraph from server
//  * @param applicationId
//  * @param params (optional)
//  * @returns the promise of getApplicationSubscriptionTransactionsCountGraph
//  */
// function getApplicationSubscriptionTransactionsCountGraph(applicationId, params) {
//   params = params || {};

//   return Restangular
//     .one('applications', applicationId)
//     .all('stats/subscription-transactions/count-graph')
//     .customGET('', params);
// }

// /**
//  * @name getApplicationSubscriptionTransactionsCount
//  * @desc Queries getApplicationSubscriptionTransactionsCount from server
//  * @param applicationId
//  * @returns the promise of getApplicationSubscriptionTransactionsCount
//  */
// function getApplicationSubscriptionTransactionsCount(applicationId) {
//   return Restangular
//     .one('applications', applicationId)
//     .all('stats/subscription-transactions/count')
//     .get();
// }

// /**
//  * @name getApplicationSubscriptionTransactionsValueAvg
//  * @desc Queries getApplicationSubscriptionTransactionsValueAvg from server
//  * @param applicationId
//  * @returns the promise of getApplicationSubscriptionTransactionsValueAvg
//  */
// function getApplicationSubscriptionTransactionsValueAvg(applicationId) {
//   return Restangular
//     .one('applications', applicationId)
//     .all('stats/subscription-transactions/value-avg')
//     .get();
// }

// /**
//  * @name getApplicationSubscriptionTransactionsValueGraph
//  * @desc Queries getApplicationSubscriptionTransactionsValueGraph from server
//  * @param applicationId
//  * @param params (optional)
//  * @returns the promise of getApplicationSubscriptionTransactionsValueGraph
//  */
// function getApplicationSubscriptionTransactionsValueGraph(applicationId, params) {
//   params = params || {};

//   return Restangular
//     .one('applications', applicationId)
//     .all('stats/subscription-transactions/value-graph')
//     .customGET('', params);
// }

// /**
//  * @name getApplicationSubscriptionTransactionsValue
//  * @desc Queries getApplicationSubscriptionTransactionsValue from server
//  * @param applicationId
//  * @returns the promise of getApplicationSubscriptionTransactionsValue
//  */
// function getApplicationSubscriptionTransactionsValue(applicationId) {
//   return Restangular
//     .one('applications', applicationId)
//     .all('stats/subscription-transactions/value')
//     .get();
// }

//   }
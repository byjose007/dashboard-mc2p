import { Injectable } from '@angular/core';
import { WrapperService } from 'src/app/services/wrapper/wrapper.service';
import { SearchService } from 'src/app/services/search/search.service';
import { Observable } from 'rxjs';
import { ApplicationsSalesService } from 'src/app/services/applications/applications-sales.service';
import { GenericService } from 'src/app/services/generic.service';
import { CurrenciesService } from 'src/app/services/currencies/currencies.service';
import { ValidateService } from 'src/app/services/validate/validate.service';
import { Router } from '@angular/router';
import { ApplicationsPlansService } from 'src/app/services/applications/applications-plans.service';

@Injectable({
  providedIn: 'root'
})
export class PrivateService {


  // legalPageList = LegalPageList;

  unitList: any;
  // loggedUser = loggedUser;

  private itemModel: any = {};
  application: any;
  errors: any[] = [];
  itemsList: any;
  heroes: any;
  totalItems: number = 0;
  itemsPerPage: number;
  query: any = {};


  // Sidebar
  //--------------------------------
  isOpenSidebar = false;


  constructor(private searchService: SearchService, private service: GenericService,
    private currenciesService: CurrenciesService, private router: Router,
    private validateService: ValidateService, private plansService: ApplicationsPlansService) {
    //  this.currencyList = this.currenciesService.getCurrencies();
    this.unitList = this.plansService.getUnitChoices();
  }

  getIdApplications(): any {
    return this.application;
  }

  setIdApplications(application: any) {
    this.application = application;
  }

  getItemModel() {
    return this.itemModel;
  }

  setItemModel(item: any) {
    this.itemModel = {};
    this.itemModel = item;
  }


  toogleSidebar() {
    this.isOpenSidebar = !this.isOpenSidebar;
  };



  // Actions
  //--------------------------------

  refreshItemsList(service, page) {
    let params: any = {};
    this.itemsList = {};
    this.query.page = page;

    // Building query params to make the corresponding request to the server
    params = this.searchService.buildParamsFromQuery(this.query); 
    service.list(this.application.id, params).subscribe(
      (responseList) => {
        this.itemsList = responseList.results;
        this.totalItems = responseList.count;
        this.itemsPerPage = responseList.items_per_page;
        // Update current state url
        // $state.go($state.current.name, $state.params, {
        //   notify: false
        // });

      });
  }


  submit(formData, redirectTo, service) {
    this.errors = [];
    // Running form validation
    this.validateService.formValidation(formData, this.errors);
    if (formData.valid) {
      // Saving changes
      service.save(this.application.id, formData.value).subscribe(
        () => {
          // success case
          this.router.navigate(redirectTo);
        }, (e) => {
          // error case
          this.errors = e.error;
        });
    }
  }

  delete(itemId, service) {
    service.delete(this.application.id, itemId).subscribe(() => {
      this.refreshItemsList(service, 1);
    })
  }
}





    /*

    this.clone = function (itemId, redirectTo) {
      // Getting service resource
      var service = WrapperService.getService($state.current.name);
      // Cloning $object
      service.clone($state.params.id, itemId)
        .then(function (response) {
          $state.go(redirectTo, {
            id: $state.params.id,
            pk: response.id
          });
        });
    };




    this.logout = function () {
      AuthenticationService.logout()
        .then(function () {
          // success case
          $state.go('public.sign.login');
        }, function (err) {
          // error case
          this.errors = err.data;
        });
    };


    // Events listeners
    //--------------------------------

    $scope.$on('UpdateApplication', function (e, application) {
      this.application = application;
    });

    $scope.$on('UpdateItemModel', function () {
      this.errors = [];
      this.itemModel = {};

      if (angular.isDefined($state.params.pk)) {
        var service = WrapperService.getService($state.current.name);
        this.itemModel = service.read($state.params.id, $state.params.pk).$object;
      }
    });

    $scope.$on('UpdateItemList', function (e, list) {
      this.query = SearchService.initQuery();
      this.itemsList = list;
      this.refreshItemsList();
    });

  


}






(function () {
  'use strict';

  angular
    .module('brocadashboard.private', [
      'brocadashboard.private.user',
      'brocadashboard.private.billing',
      'brocadashboard.private.applications',
    ])
    .config(privateConfig);

  privateConfig.$inject = ['$stateProvider'];
  function privateConfig ($stateProvider) {
    $stateProvider
      .state('private', {
        abstract: true,
        url: '',
        templateUrl: 'apps/private/private.tpl.html',
        controller: privateController,
        controllerAs: '$privateCtrl',
        resolve: {
          loggedUser: ['AuthenticationService', function (AuthenticationService) {
            return AuthenticationService.getUser();
          }],
          currencyList: ['CurrenciesService', function (CurrenciesService) {
            return CurrenciesService.getCurrencies();
          }],
          unitList: ['ApplicationsPlansService', function (PlansService) {
            return PlansService.getUnitChoices();
          }],
          LegalPageList: ['Settings', 'SCreatorService', function (Settings, SCreatorService) {
            return SCreatorService.getSimplePages(Settings.LEGAL_SITE_ID, {'show_in_menu': 1});
          }]
        }
      });
  }
*/

import { Injectable } from '@angular/core';
import { isDefined } from '@angular/compiler/src/util';
import { GenericService } from 'src/app/services/generic.service';
import { Observable } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Constants } from 'src/app/constants';



@Injectable({
  providedIn: 'root'
})
export class ApplicationsSalesService {


  constructor(private service: GenericService) {

  }

  read(applicationId, id) {
    return this.getApplicationSale(applicationId, id);
  }

  list(applicationId, params: any = {}) {
    return this.getApplicationSales(applicationId, params);
  }


  /**
    * @name _buildFormData
    * @desc build FormData object to send on the request to the server
    * @param params (optional)
    * @returns the object of FormData
    */
  _buildFormData(params: any = {}) {
    let fd = new FormData();

    if (isDefined(params.buyerCard)) {
      fd.append('buyer_card', params.buyerCard);
    }
    if (isDefined(params.gatewayTransactionId)) {
      fd.append('gateway_transaction_id', params.gatewayTransactionId);
    }
    if (isDefined(params.application)) {
      fd.append('application', params.application);
    }
    if (isDefined(params.buyerAddressCity)) {
      fd.append('buyer_address_city', params.buyerAddressCity);
    }
    if (isDefined(params.buyerAddressState)) {
      fd.append('buyer_address_state', params.buyerAddressState);
    }
    if (isDefined(params.buyerCountry)) {
      fd.append('buyer_country', params.buyerCountry);
    }
    if (isDefined(params.buyerCardExpirationDate)) {
      fd.append('buyer_card_expiration_date', params.buyerCardExpirationDate);
    }
    if (isDefined(params.accountId)) {
      fd.append('account_id', params.accountId);
    }
    if (isDefined(params.buyerAddressStreet)) {
      fd.append('buyer_address_street', params.buyerAddressStreet);
    }
    if (isDefined(params.buyerAddressName)) {
      fd.append('buyer_address_name', params.buyerAddressName);
    }
    if (isDefined(params.buyerCompany)) {
      fd.append('buyer_company', params.buyerCompany);
    }
    if (isDefined(params.paymentType)) {
      fd.append('payment_type', params.paymentType);
    }
    if (isDefined(params.accountType)) {
      fd.append('account_type', params.accountType);
    }
    if (isDefined(params.buyerAddressCountry)) {
      fd.append('buyer_address_country', params.buyerAddressCountry);
    }
    if (isDefined(params.buyerFirstName)) {
      fd.append('buyer_first_name', params.buyerFirstName);
    }
    if (isDefined(params.buyerEmail)) {
      fd.append('buyer_email', params.buyerEmail);
    }
    if (isDefined(params.buyerId)) {
      fd.append('buyer_id', params.buyerId);
    }
    if (isDefined(params.transaction)) {
      fd.append('transaction', params.transaction);
    }
    if (isDefined(params.amount)) {
      fd.append('amount', params.amount);
    }
    if (isDefined(params.fee)) {
      fd.append('fee', params.fee);
    }
    if (isDefined(params.buyerAddressCountryCode)) {
      fd.append('buyer_address_country_code', params.buyerAddressCountryCode);
    }
    if (isDefined(params.buyerAddressStatus)) {
      fd.append('buyer_address_status', params.buyerAddressStatus);
    }
    if (isDefined(params.buyerLastName)) {
      fd.append('buyer_last_name', params.buyerLastName);
    }
    if (isDefined(params.buyerAddressZip)) {
      fd.append('buyer_address_zip', params.buyerAddressZip);
    }
    if (isDefined(params.buyerContactPhone)) {
      fd.append('buyer_contact_phone', params.buyerContactPhone);
    }
    if (isDefined(params.buyerCardCardholderName)) {
      fd.append('buyer_card_cardholder_name', params.buyerCardCardholderName);
    }

    return fd;
  }
  /**
   * @name createApplicationSale
   * @desc Queries createApplicationSale from server
   * @param applicationId
   * @param params (optional)
   * @returns the promise of createApplicationSale
   */
  createApplicationSale(applicationId, params: any = {}) {
    let fd = this._buildFormData(params);
    return this.service.create(`applications/${applicationId}/sales/`, fd);
  }

  /**
   * @name getApplicationSales
   * @desc Queries getApplicationSales from server
   * @param applicationId
   * @param params (optional)
   * @returns the promise of getApplicationSales
   */
  getApplicationSales(applicationId, params: any = {}) {
    return this.service.getAllCustom(`applications/${applicationId}/sales/`, params);
  }

  /**
   * @name getApplicationSale
   * @desc Queries getApplicationSale from server
   * @param applicationId
   * @param id
   * @returns the promise of getApplicationSale
   */
  getApplicationSale(applicationId, id) {
    return this.service.getCustom('applications', applicationId, 'sales', id);
  }

  /**
   * @name updateApplicationSale
   * @desc Queries updateApplicationSale from server
   * @param applicationId
   * @param id
   * @returns the promise of updateApplicationSale
   */
  updateApplicationSale(applicationId, id, params: any = {}) {
    let fd = this._buildFormData(params);
    return this.service.update(`applications/${applicationId}/sales/`, id, fd);
  }

  /**
   * @name deleteApplicationSale
   * @desc Queries deleteApplicationSale from server
   * @param applicationId
   * @param id
   * @returns the promise of deleteApplicationSale
   */
  deleteApplicationSale(applicationId, id) {
    return this.service.remove(`applications/${applicationId}/sales/`, id);
  }

  /**
   * @name captureApplicationSale
   * @desc Queries captureApplicationSale from server
   * @param applicationId
   * @param id
   * @returns the promise of captureApplicationSale
   */
  captureApplicationSale(applicationId, id) {
    return this.service.create(`applications/${applicationId}/sales/${id}/capture/`)
  }

  /**
   * @name voidApplicationSale
   * @desc Queries voidApplicationSale from server
   * @param applicationId
   * @param id
   * @returns the promise of voidApplicationSale
   */
  voidApplicationSale(applicationId, id) {
    return this.service.create(`applications/${applicationId}/sales/${id}/void/`)
  }

  /**
   * @name refundApplicationSale
   * @desc Queries refundApplicationSale from server
   * @param applicationId
   * @param id
   * @returns the promise of refundApplicationSale
   */
  refundApplicationSale(applicationId, id) {
    return this.service.create(`applications/${applicationId}/sales/${id}/refund/`);
  }

}

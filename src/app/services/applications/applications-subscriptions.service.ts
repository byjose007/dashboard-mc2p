import { Injectable } from '@angular/core';
import { isDefined } from '@angular/compiler/src/util';
import { GenericService } from 'src/app/services/generic.service';

@Injectable({
  providedIn: 'root'
})
export class ApplicationsSubscriptionsService {

  constructor(private service: GenericService) { }

  read(applicationId, id) {
    return this.getApplicationSubscriptionTransaction(applicationId, id);
  }

  list(applicationId, params: any = {}) {
    return this.getApplicationSubscriptionTransactions(applicationId, params);
  }



  /**
   * @name _buildSubscriptionTransactionsFormData
   * @desc build FormData object to send on the request to the server
   * @param params (optional)
   * @returns the object of FormData
   */
  _buildSubscriptionTransactionsFormData(params) {
    var fd = new FormData();

    if (isDefined(params.trialPrice2)) {
      fd.append('trial_price2', params.trialPrice2);
    }
    if (isDefined(params.note)) {
      fd.append('note', params.note);
    }
    if (isDefined(params.application)) {
      fd.append('application', params.application);
    }
    if (isDefined(params.orderType)) {
      fd.append('order_type', params.orderType);
    }
    if (isDefined(params.itemsPrice)) {
      fd.append('items_price', params.itemsPrice);
    }
    if (isDefined(params.language)) {
      fd.append('language', params.language);
    }
    if (isDefined(params.status)) {
      fd.append('status', params.status);
    }
    if (isDefined(params.returnUrl)) {
      fd.append('return_url', params.returnUrl);
    }
    if (isDefined(params.unit2)) {
      fd.append('unit2', params.unit2);
    }
    if (isDefined(params.subscription)) {
      fd.append('subscription', params.subscription);
    }
    if (isDefined(params.totalPrice)) {
      fd.append('total_price', params.totalPrice);
    }
    if (isDefined(params.notifyUrl)) {
      fd.append('notify_url', params.notifyUrl);
    }
    if (isDefined(params.gatewaySelected)) {
      fd.append('gateway_selected', params.gatewaySelected);
    }
    if (isDefined(params.fromOrder)) {
      fd.append('from_order', params.fromOrder);
    }
    if (isDefined(params.currency)) {
      fd.append('currency', params.currency);
    }
    if (isDefined(params.duration2)) {
      fd.append('duration2', params.duration2);
    }
    if (isDefined(params.unit1)) {
      fd.append('unit1', params.unit1);
    }
    if (isDefined(params.gateways)) {
      fd.append('gateways', params.gateways);
    }
    if (isDefined(params.trialPrice1)) {
      fd.append('trial_price1', params.trialPrice1);
    }
    if (isDefined(params.token)) {
      fd.append('token', params.token);
    }
    if (isDefined(params.orderId)) {
      fd.append('order_id', params.orderId);
    }
    if (isDefined(params.duration1)) {
      fd.append('duration1', params.duration1);
    }
    if (isDefined(params.cancelUrl)) {
      fd.append('cancel_url', params.cancelUrl);
    }

    return fd;
  }

  /**
   * @name createApplicationSubscriptionTransaction
   * @desc Queries createApplicationSubscriptionTransaction from server
   * @param applicationId
   * @param params (optional)
   * @returns the promise of createApplicationSubscriptionTransaction
   */
  createApplicationSubscriptionTransaction(applicationId, params: any = {}) {
    let fd = this._buildSubscriptionTransactionsFormData(params);
    return this.service.create(`applications/${applicationId}/subscription-transactions/`, fd);
  }

  /**
   * @name getApplicationSubscriptionTransactions
   * @desc Queries getApplicationSubscriptionTransactions from server
   * @param applicationId
   * @param params (optional)
   * @returns the promise of getApplicationSubscriptionTransactions
   */
  getApplicationSubscriptionTransactions(applicationId, params: any = {}) {
    return this.service.getAllCustom(`applications/${applicationId}/subscription-transactions/`, params);
  }

  /**
   * @name getDoneApplicationSubscriptionTransactions
   * @desc Queries getDoneApplicationSubscriptionTransactions from server
   * @param applicationId
   * @param params (optional)
   * @returns the promise of getDoneApplicationSubscriptionTransactions
   */
  getDoneApplicationSubscriptionTransactions(applicationId, params: any = {}) {
    params['status'] = 'D';
    return this.service.getAllCustom(`applications/${applicationId}/subscription-transactions/`, params);
  }

  /**
   * @name getApplicationSubscriptionTransaction
   * @desc Queries getApplicationSubscriptionTransaction from server
   * @param applicationId
   * @param id
   * @returns the promise of getApplicationSubscriptionTransaction
   */
  getApplicationSubscriptionTransaction(applicationId, id) {

    return this.service.getOne(`applications/${applicationId}/subscription-transactions/`, id);
  }

  /**
   * @name updateApplicationSubscriptionTransaction
   * @desc Queries updateApplicationSubscriptionTransaction from server
   * @param applicationId
   * @param id
   * @param params (optional)
   * @returns the promise of updateApplicationSubscriptionTransaction
   */
  updateApplicationSubscriptionTransaction(applicationId, id, params: any = {}) {
    var fd = this._buildSubscriptionTransactionsFormData(params);
    return this.service.update(`applications/${applicationId}/subscription-transactions/`, id, fd);

  }

  /**
   * @name deleteApplicationSubscriptionTransaction
   * @desc Queries deleteApplicationSubscriptionTransaction from server
   * @param applicationId
   * @param id
   * @returns the promise of deleteApplicationSubscriptionTransaction
   */
  deleteApplicationSubscriptionTransaction(applicationId, id) {
    return this.service.remove(`applications/${applicationId}/subscription-transactions/`, id);
  }

}

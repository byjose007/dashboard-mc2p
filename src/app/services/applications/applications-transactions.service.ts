import { Injectable } from '@angular/core';
import { isDefined } from '@angular/compiler/src/util';
import { GenericService } from 'src/app/services/generic.service';

@Injectable({
  providedIn: 'root'
})
export class ApplicationsTransactionsService {

  constructor(private service: GenericService) { }

  read(applicationId, id) {
    return this.getApplicationTransaction(applicationId, id);
  }

  list(applicationId, params: any = {}) {
    return this.getApplicationTransactions(applicationId, params);
  }

  /**
   * @name getStatusChoices
   * @desc Queries getStatusChoices from server
   * @returns the promise of getStatusChoices
   */
  getStatusChoices() {
    return [
      { name: 'Pendiente', id: 'N' },
      { name: 'Pagado', id: 'D' },
      { name: 'Cancelado', id: 'C' },
      { name: 'Expirado', id: 'E' },
    ];
  }

  /**
   * @name getSubscriptionStatusChoices
   * @desc Queries getSubscriptionStatusChoices from server
   * @returns the promise of getSubscriptionStatusChoices
   */
  getSubscriptionStatusChoices() {
    return [
      { name: 'En Espera', id: 'W' },
      { name: 'Activa', id: 'A' },
      { name: 'Pausada', id: 'P' },
      { name: 'Parada', id: 'S' },
    ];
  }

  /**
   * @name getAuthorizationStatusChoices
   * @desc Queries getAuthorizationStatusChoices from server
   * @returns the promise of getAuthorizationStatusChoices
   */
  getAuthorizationStatusChoices() {
    return [
      { name: 'Activa', id: 'A' },
      { name: 'Eliminada', id: 'R' },
    ];
  }

  /**
   * @name getApplicationTransactions
   * @desc Queries getApplicationTransactions from server
   * @param applicationId
   * @param params (optional)
   * @returns the promise of getApplicationTransactions
   */
  getApplicationTransactions(applicationId, params: any = {}) {

    return this.service.getAllCustom(`applications/${applicationId}/transactions/`, params);
  }

  /**
   * @name getApplicationTransaction
   * @desc Queries getApplicationTransaction from server
   * @param applicationId
   * @param id
   * @returns the promise of getApplicationTransaction
   */
  getApplicationTransaction(applicationId, id) {
    return this.service.getCustom('applications', applicationId, 'transactions', id);
  }

  /**
   * @name _buildOnePaymentTransactionsFormData
   * @desc build FormData object to send on the request to the server
   * @param params (optional)
   * @returns the object of FormData
   */
  _buildOnePaymentTransactionsFormData(params) {
    var fd = new FormData();

    if (isDefined(params.note)) {
      fd.append('note', params.note);
    }
    if (isDefined(params.application)) {
      fd.append('application', params.application);
    }
    if (isDefined(params.taxValue)) {
      fd.append('tax_value', params.taxValue);
    }
    if (isDefined(params.gatewaySelected)) {
      fd.append('gateway_selected', params.gatewaySelected);
    }
    if (isDefined(params.language)) {
      fd.append('language', params.language);
    }
    if (isDefined(params.shipping)) {
      fd.append('shipping', params.shipping);
    }
    if (isDefined(params.returnUrl)) {
      fd.append('return_url', params.returnUrl);
    }
    if (isDefined(params.totalPrice)) {
      fd.append('total_price', params.totalPrice);
    }
    if (isDefined(params.products)) {
      fd.append('products', params.products);
    }
    if (isDefined(params.notifyUrl)) {
      fd.append('notify_url', params.notifyUrl);
    }
    if (isDefined(params.coupon)) {
      fd.append('coupon', params.coupon);
    }
    if (isDefined(params.tax)) {
      fd.append('tax', params.tax);
    }
    if (isDefined(params.fromOrder)) {
      fd.append('from_order', params.fromOrder);
    }
    if (isDefined(params.shippingValue)) {
      fd.append('shipping_value', params.shippingValue);
    }
    if (isDefined(params.couponName)) {
      fd.append('coupon_name', params.couponName);
    }
    if (isDefined(params.taxName)) {
      fd.append('tax_name', params.taxName);
    }
    if (isDefined(params.currency)) {
      fd.append('currency', params.currency);
    }
    if (isDefined(params.shippingName)) {
      fd.append('shipping_name', params.shippingName);
    }
    if (isDefined(params.itemsPrice)) {
      fd.append('items_price', params.itemsPrice);
    }
    if (isDefined(params.couponValue)) {
      fd.append('coupon_value', params.couponValue);
    }
    if (isDefined(params.orderType)) {
      fd.append('order_type', params.orderType);
    }
    if (isDefined(params.gateways)) {
      fd.append('gateways', params.gateways);
    }
    if (isDefined(params.token)) {
      fd.append('token', params.token);
    }
    if (isDefined(params.orderId)) {
      fd.append('order_id', params.orderId);
    }
    if (isDefined(params.cancelUrl)) {
      fd.append('cancel_url', params.cancelUrl);
    }
    if (isDefined(params.status)) {
      fd.append('status', params.status);
    }

    return fd;
  }

  /**
   * @name createApplicationOnePaymentTransaction
   * @desc Queries createApplicationOnePaymentTransaction from server
   * @param applicationId
   * @param params (optional)
   * @returns the promise of createApplicationOnePaymentTransaction
   */
  createApplicationOnePaymentTransaction(applicationId, params: any = {}) {
    let fd = this._buildOnePaymentTransactionsFormData(params);
    return this.service.create(`applications/${applicationId}/one-payment-transactions/`, fd);
  }

  /**
   * @name getApplicationOnePaymentTransactions
   * @desc Queries getApplicationOnePaymentTransactions from server
   * @param applicationId
   * @param params (optional)
   * @returns the promise of getApplicationOnePaymentTransactions
   */
  getApplicationOnePaymentTransactions(applicationId, params: any = {}) {
    return this.service.getAllCustom(`applications/${applicationId}/one-payment-transactions/`, params);
  }


  /**
   * @name getApplicationOnePaymentTransaction
   * @desc Queries getApplicationOnePaymentTransaction from server
   * @param applicationId
   * @param id
   * @returns the promise of getApplicationOnePaymentTransaction
   */
  getApplicationOnePaymentTransaction(applicationId, id) {
    return this.service.getOne(`applications/${applicationId}/one-payment-transactions/`, id);
  }

  /**
   * @name updateApplicationOnePaymentTransaction
   * @desc Queries updateApplicationOnePaymentTransaction from server
   * @param applicationId
   * @param id
   * @param params (optional)
   * @returns the promise of updateApplicationOnePaymentTransaction
   */
  updateApplicationOnePaymentTransaction(applicationId, id, params: any = {}) {
    var fd = this._buildOnePaymentTransactionsFormData(params);
    return this.service.update(`applications/${applicationId}/one-payment-transactions/`, id, fd);
  }

  /**
   * @name deleteApplicationOnePaymentTransaction
   * @desc Queries deleteApplicationOnePaymentTransaction from server
   * @param applicationId
   * @param id
   * @returns the promise of deleteApplicationOnePaymentTransaction
   */
  deleteApplicationOnePaymentTransaction(applicationId, id) {

    return this.service.remove(`applications/${applicationId}/one-payment-transactions/`, id);
  }




  /**
   * @name getApplicationAuthorizationTransactions
   * @desc Queries getApplicationAuthorizationTransactions from server
   * @param applicationId
   * @param params (optional)
   * @returns the promise of getApplicationAuthorizationTransactions
   */
  getApplicationAuthorizationTransactions(applicationId, params: any = {}) {


    return this.service.getAllCustom(`applications/${applicationId}/authorization-transactions/`, params);
  }

  /**
   * @name getDoneApplicationAuthorizationTransactions
   * @desc Queries getDoneApplicationAuthorizationTransactions from server
   * @param applicationId
   * @param params (optional)
   * @returns the promise of getDoneApplicationAuthorizationTransactions
   */
  getDoneApplicationAuthorizationTransactions(applicationId, params: any = {}) {
    params['status'] = 'D';
    return this.service.getAllCustom(`applications/${applicationId}/authorization-transactions/`, params);
  }

  /**
   * @name getApplicationAuthorizationTransaction
   * @desc Queries getApplicationAuthorizationTransaction from server
   * @param applicationId
   * @param id
   * @returns the promise of getApplicationAuthorizationTransaction
   */
  getApplicationAuthorizationTransaction(applicationId, id) {
    return this.service.getOne(`applications/${applicationId}/authorization-transactions/`, id);
  }



}





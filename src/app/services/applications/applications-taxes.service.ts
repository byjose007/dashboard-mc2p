import { Injectable } from '@angular/core';
import { isDefined } from '@angular/compiler/src/util';
import { GenericService } from 'src/app/services/generic.service';
import { isUndefined } from 'util';

@Injectable({
  providedIn: 'root'
})
export class ApplicationsTaxesService {

  constructor(private service: GenericService) { }

  read(applicationId, id) {
    return this.getApplicationTax(applicationId, id);
  }

  list(applicationId, params: any = {}) {
    return this.getApplicationTaxes(applicationId, params);
  }

  save(applicationId, productData) {
    return this.saveApplicationTax(applicationId, productData);
  }

  delete(applicationId, id){
    return this.deleteApplicationTax(applicationId,id);
  }



  /**
 * @name _buildFormData
 * @desc build FormData object to send on the request to the server
 * @param params (optional)
 * @returns the object of FormData
 */
  _buildFormData(params) {
    let fd = new FormData();

    if (isDefined(params.name)) {
      fd.append('name', params.name);
    }
    if (isDefined(params.percent)) {
      fd.append('percent', params.percent);
    }

    return fd;
  }

  /**
   * @name createApplicationTax
   * @desc Queries createApplicationTax from server
   * @param applicationId
   * @param params (optional)
   * @returns the promise of createApplicationTax
   */
  createApplicationTax(applicationId, params: any = {}) {
    // let fd = this._buildFormData(params);
    return this.service.create(`applications/${applicationId}/taxes/`, params);
  }

  /**
   * @name getApplicationTaxes
   * @desc Queries getApplicationTaxes from server
   * @param applicationId
   * @param params (optional)
   * @returns the promise of getApplicationTaxes
   */
  getApplicationTaxes(applicationId, params: any = {}) {
    return this.service.getAllCustom(`applications/${applicationId}/taxes/`, params);
  }

  /**
   * @name getApplicationTax
   * @desc Queries getApplicationTax from server
   * @param applicationId
   * @param id
   * @returns the promise of getApplicationTax
   */
  getApplicationTax(applicationId, id) {
    
    return this.service.getOne(`applications/${applicationId}/taxes/`, id);
  }

  /**
   * @name updateApplicationTax
   * @desc Queries updateApplicationTax from server
   * @param applicationId
   * @param id
   * @param params (optional)
   * @returns the promise of updateApplicationTax
   */
  updateApplicationTax(applicationId, id, params: any = {}) {
    // let fd = this._buildFormData(params);
    return this.service.update(`applications/${applicationId}/taxes/`, id, params);
  }

  /**
   * @name cloneApplicationTax
   * @desc Queries cloneApplicationTax from server
   * @param applicationId
   * @param id
   * @returns the promise of cloneApplicationTax
   */
  cloneApplicationTax(applicationId, id) {
    return this.service.create(`applications/${applicationId}/taxes/${id}/clone/`);
  }

  /**
   * @name deleteApplicationTax
   * @desc Queries deleteApplicationTax from server
   * @param applicationId
   * @param id
   * @returns the promise of deleteApplicationTax
   */
  deleteApplicationTax(applicationId, id) {
    return this.service.remove(`applications/${applicationId}/taxes/`, id);
  }

  /**
   * @name saveApplicationTax
   * @desc Queries saveApplicationTax from server
   * @param applicationId
   * @param params (optional)
   * @returns the promise of saveApplicationTax
   */
  saveApplicationTax(applicationId, productData) {
    let productId = productData.id;

    if (productId==null) {
      return this.createApplicationTax(applicationId, productData);
    } else {
      return this.updateApplicationTax(applicationId, productId, productData);
    }
  }
}
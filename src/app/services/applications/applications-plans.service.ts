import { Injectable } from '@angular/core';
import { isObject, isUndefined } from 'util';
import { isDefined } from '@angular/compiler/src/util';
import { GenericService } from 'src/app/services/generic.service';

@Injectable({
  providedIn: 'root'
})
export class ApplicationsPlansService {

  constructor(private service: GenericService) { }

  read(applicationId, id) {
    return this.getApplicationPlan(applicationId, id);
  }

  list(applicationId, params: any = {}) {
    return this.getApplicationPlans(applicationId, params);
  }


  save(applicationId, subscriptionData) {
    return this.saveApplicationPlan(applicationId, subscriptionData);
  }

  delete(applicationId, id){
    return this.deleteApplicationPlan(applicationId,id);
  }


  /**
   * @name getUnitChoices
   * @desc Queries getUnitChoices from server
   * @returns the promise of getUnitChoices
   */
  getUnitChoices() {
    return [
      { id: 'D', text: 'Día' },
      { id: 'M', text: 'Mes' },
      { id: 'A', text: 'Año' }
    ];
  }

  /**
   * @name _buildFormData
   * @desc build FormData object to send on the request to the server
   * @param params (optional)
   * @returns the object of FormData
   */
  _buildFormData(params) {
    let fd = new FormData();

    if (isDefined(params.unit)) {
      if (isObject(params.unit) && isDefined(params.unit.id)) {
        fd.append('unit', params.unit.id);
      } else {
        fd.append('unit', params.unit);
      }
    }
    if (isDefined(params.name)) {
      fd.append('name', params.name);
    }
    if (isDefined(params.description)) {
      fd.append('description', params.description);
    }
    if (isDefined(params.duration)) {
      fd.append('duration', params.duration);
    }
    if (isDefined(params.price)) {
      fd.append('price', params.price);
    }
    if (isDefined(params.plan_id)) {
      fd.append('plan_id', params.plan_id);
    }
    if (isDefined(params.recurring)) {
      fd.append('recurring', params.recurring);
    }

    return fd;
  }


  /**
   * @name createApplicationPlan
   * @desc Queries createApplicationPlan from server
   * @param applicationId
   * @param params (optional)
   * @returns the promise of createApplicationPlan
   */
  createApplicationPlan(applicationId, params: any = {}) {
    // let fd = this._buildFormData(params);
    return this.service.create(`applications/${applicationId}/plans/`, params);
  }

  /**
   * @name getApplicationPlans
   * @desc Queries getApplicationPlans from server
   * @param applicationId
   * @param params (optional)
   * @returns the promise of getApplicationPlans
   */
  getApplicationPlans(applicationId, params: any = {}) {
    return this.service.getAllCustom(`applications/${applicationId}/plans/`, params);
  }

  /**
   * @name getApplicationPlan
   * @desc Queries getApplicationPlan from server
   * @param applicationId
   * @param id
   * @returns the promise of getApplicationPlan
   */
  getApplicationPlan(applicationId, id) {
    return this.service.getOne(`applications/${applicationId}/plans/`, id);
  }

  /**
   * @name updateApplicationPlan
   * @desc Queries updateApplicationPlan from server
   * @param applicationId
   * @param id
   * @param params (optional)
   * @returns the promise of updateApplicationPlan
   */
  updateApplicationPlan(applicationId, id, params: any = {}) {
    // let fd = this._buildFormData(params);
    return this.service.update(`applications/${applicationId}/plans/`, id, params);
  }

  /**
   * @name cloneApplicationPlan
   * @desc Queries cloneApplicationPlan from server
   * @param applicationId
   * @param id
   * @returns the promise of cloneApplicationPlan
   */
  cloneApplicationPlan(applicationId, id) {
    return this.service.create(`applications/${applicationId}/plans/${id}/clone/`);
  }

  /**
   * @name deleteApplicationPlan
   * @desc Queries deleteApplicationPlan from server
   * @param applicationId
   * @param id
   * @returns the promise of deleteApplicationPlan
   */
  deleteApplicationPlan(applicationId, id) {
    return this.service.remove(`applications/${applicationId}/plans/`, id);
  }

  /**
   * @name saveApplicationPlan
   * @desc Queries saveApplicationPlan from server
   * @param applicationId
   * @param params (optional)
   * @returns the promise of saveApplicationPlan
   */
  saveApplicationPlan(applicationId, subscriptionData) {
    let subscriptionId = subscriptionData.id;

    if (subscriptionId == null) {
      return this.createApplicationPlan(applicationId, subscriptionData);
    } else {
      return this.updateApplicationPlan(applicationId, subscriptionId, subscriptionData);
    }
  }
}

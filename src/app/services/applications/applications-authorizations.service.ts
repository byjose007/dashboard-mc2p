import { Injectable } from '@angular/core';
import { ApplicationsTransactionsService } from 'src/app/services/applications/applications-transactions.service';

@Injectable({
  providedIn: 'root'
})
export class ApplicationsAuthorizationsService {

  constructor(private transactionsService: ApplicationsTransactionsService) { }

  read(applicationId, id) {
    return this.transactionsService.getApplicationAuthorizationTransaction(applicationId,id);
  }

  list(applicationId, params: any = {}) {
    return this.transactionsService.getApplicationAuthorizationTransactions(applicationId,params);
  }

}

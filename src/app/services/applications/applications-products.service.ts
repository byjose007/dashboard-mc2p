import { Injectable } from '@angular/core';
import { isDefined } from '@angular/compiler/src/util';
import { isUndefined } from 'util';
import { GenericService } from 'src/app/services/generic.service';

@Injectable({
  providedIn: 'root'
})
export class ApplicationsProductsService {

  constructor(private service: GenericService) { }


  read(applicationId, id) {
    return this.getApplicationProduct(applicationId, id);
  }

  list(applicationId, params: any = {}) {
    return this.getApplicationProducts(applicationId, params);
  }


  save(applicationId, productData) {
    return this.saveApplicationProduct(applicationId, productData);
  }

  delete(applicationId, id){
    return this.deleteApplicationProduct(applicationId,id);
  }

    /**
     * @name this._buildFormData
     * @desc build FormData object to send on the request to the server
     * @param params (optional)
     * @returns the object of FormData
     */
    _buildFormData (params) {
      let fd = new FormData();

      if (isDefined(params.product_id)) {
        fd.append('product_id', params.product_id);
      }
      if (isDefined(params.name)) {
        fd.append('name', params.name);
      }
      if (isDefined(params.description)) {
        fd.append('description', params.description);
      }
      if (isDefined(params.price)) {
        fd.append('price', params.price);
      }

      return fd;
    }


     /**
     * @name createApplicationProduct
     * @desc Queries createApplicationProduct from server
     * @param applicationId
     * @param params (optional)
     * @returns the promise of createApplicationProduct
     */
    createApplicationProduct (applicationId, params: any = {}) {
      // let fd = this._buildFormData(params);
      // console.log(fd.getAll);

      return this.service.create(`applications/${applicationId}/products/`, params);
    }

    /**
     * @name getApplicationProducts
     * @desc Queries getApplicationProducts from server
     * @param applicationId
     * @param params (optional)
     * @returns the promise of getApplicationProducts
     */
    getApplicationProducts (applicationId, params: any = {}) {
      return this.service.getAllCustom(`applications/${applicationId}/products/`, params);
    }


    /**
     * @name getApplicationProduct
     * @desc Queries getApplicationProduct from server
     * @param applicationId
     * @param id
     * @returns the promise of getApplicationProduct
     */
     getApplicationProduct (applicationId, id) {
      return this.service.getOne(`applications/${applicationId}/products/`, id);
    }

    /**
     * @name updateApplicationProduct
     * @desc Queries updateApplicationProduct from server
     * @param applicationId
     * @param id
     * @param params (optional)
     * @returns the promise of updateApplicationProduct
     */
     updateApplicationProduct (applicationId, id, params: any = {}) {

      // let fd = this._buildFormData(params);

      return this.service.update(`applications/${applicationId}/products/`, id, params);
    }

    /**
     * @name saveApplicationProduct
     * @desc Queries saveApplicationProduct from server
     * @param applicationId
     * @param params (optional)
     * @returns the promise of saveApplicationProduct
     */
     saveApplicationProduct (applicationId, productData) {
      let productId = productData.id;

      if (productId == null ) {
        return this.createApplicationProduct(applicationId, productData);
      } else {
        return this.updateApplicationProduct(applicationId, productId, productData);
      }
    }

    /**
     * @name cloneApplicationProduct
     * @desc Queries cloneApplicationProduct from server
     * @param applicationId
     * @param id
     * @returns the promise of cloneApplicationProduct
     */
     cloneApplicationProduct (applicationId, id) {
      return this.service.create(`applications/${applicationId}/products/${id}/clone/`);
    }

    /**
     * @name deleteApplicationProduct
     * @desc Queries deleteApplicationProduct from server
     * @param applicationId
     * @param id
     * @returns the promise of deleteApplicationProduct
     */
     deleteApplicationProduct (applicationId, id) {
      return this.service.remove(`applications/${applicationId}/products/`, id);
    }

}


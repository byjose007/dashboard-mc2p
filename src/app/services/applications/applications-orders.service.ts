import { Injectable } from '@angular/core';
import { GenericService } from 'src/app/services/generic.service';
import { ENGINE_METHOD_NONE } from 'constants';
import { isDefined } from '@angular/compiler/src/util';

@Injectable({
  providedIn: 'root'
})
export class ApplicationsOrdersService {
  constructor(private service: GenericService) { }

  read(applicationId, id) {
    return this.getApplicationOrder(applicationId, id);
  }

  list(applicationId, params: any = {}) {
    return this.getApplicationOrders(applicationId, params);
  }



  /**
     * @name getApplicationOrders
     * @desc Queries getApplicationOrders from server
     * @param applicationId
     * @param params (optional)
     * @returns the promise of getApplicationOrders
     */
  getApplicationOrders(applicationId, params: any = {}) {
    return this.service.getAllCustom(`applications/${applicationId}/orders/`, params);
  }

  /**
   * @name getApplicationOrder
   * @desc Queries getApplicationOrder from server
   * @param applicationId
   * @param id
   * @returns the promise of getApplicationOrder
   */
  getApplicationOrder(applicationId, id) {
    return this.service.getCustom('applications', applicationId, 'orders/', id);
  }

  /**
   * @name cloneApplicationOrder
   * @desc Queries cloneApplicationOrder from server
   * @param applicationId
   * @param id
   * @returns the promise of cloneApplicationOrder
   */
  cloneApplicationOrder(applicationId, id) {
    return this.service.create(`applications/${applicationId}/orders/${id}/clone/`);
  }

  /**
   * @name deleteApplicationOrder
   * @desc Queries deleteApplicationOrder from server
   * @param applicationId
   * @param id
   * @returns the promise of deleteApplicationOrder
   */
  deleteApplicationOrder(applicationId, id) {
    return this.service.remove(`applications/${applicationId}/orders/`, id);
  }

  /**
   * @name saveApplicationOrder
   * @desc Queries saveApplicationOrder from server
   * @param applicationId
   * @param orderData
   * @returns the promise of saveApplicationOrder
   */
  saveApplicationOrder(applicationId, orderData) {
    let orderId = orderData.id;
    let orderType = orderData.order_type;

    if (isDefined(orderId)) {
      let updatePromise = (orderType === 'P') ? this.updateApplicationOnePaymentOrder : this.updateApplicationSubscriptionOrder;
      return updatePromise(applicationId, orderId, orderData);
    } else {
      let createPromise = (orderType === 'P') ? this.createApplicationOnePaymentOrder : this.createApplicationSubscriptionOrder;
      return createPromise(applicationId, orderData);
    }
  }

  /**
   * @name getApplicationOnePaymentOrders
   * @desc Queries getApplicationOnePaymentOrders from server
   * @param applicationId
   * @param params (optional)
   * @returns the promise of getApplicationOnePaymentOrders
   */
  getApplicationOnePaymentOrders(applicationId, params: any = {}) {
    return this.service.getAllCustom(`applications/${applicationId}/one-payment-orders/`, params);
  }

  /**
   * @name createApplicationOnePaymentOrder
   * @desc Queries createApplicationOnePaymentOrder from server
   * @param applicationId
   * @param params (optional)
   * @returns the promise of createApplicationOnePaymentOrder
   */
  createApplicationOnePaymentOrder(applicationId, params: any = {}) {
    return this.service.create(`applications/${applicationId}/one-payment-orders/`, params);
  }

  /**
   * @name getApplicationOnePaymentOrder
   * @desc Queries getApplicationOnePaymentOrder from server
   * @param applicationId
   * @param id
   * @returns the promise of getApplicationOnePaymentOrder
   */
  getApplicationOnePaymentOrder(applicationId, id) {
    return this.service.getOne(`applications/${applicationId}/one-payment-orders/`, id);
  }


  /**
   * @name updateApplicationOnePaymentOrder
   * @desc Queries updateApplicationOnePaymentOrder from server
   * @param applicationId
   * @param id
   * @param params (optional)
   * @returns the promise of updateApplicationOnePaymentOrder
   */
  updateApplicationOnePaymentOrder(applicationId, id, params: any = {}) {
    return this.service.update(`applications/${applicationId}/one-payment-orders/`, id, params);
  }


  /**
   * @name cloneApplicationOnePaymentOrder
   * @desc Queries cloneApplicationOnePaymentOrder from server
   * @param applicationId
   * @param id
   * @returns the promise of cloneApplicationOnePaymentOrder
   */
  cloneApplicationOnePaymentOrder(applicationId, id) {
    return this.service.create(`applications/${applicationId}/one-payment-orders/${id}/clone/`);
  }

  /**
   * @name deleteApplicationOnePaymentOrder
   * @desc Queries deleteApplicationOnePaymentOrder from server
   * @param applicationId
   * @param id
   * @returns the promise of deleteApplicationOnePaymentOrder
   */
  deleteApplicationOnePaymentOrder(applicationId, id) {
    return this.service.remove(`applications/${applicationId}/one-payment-orders/`, id);
  }

  /**
   * @name createApplicationSubscriptionOrder
   * @desc Queries createApplicationSubscriptionOrder from server
   * @param applicationId
   * @param params (optional)
   * @returns the promise of createApplicationSubscriptionOrder
   */
  createApplicationSubscriptionOrder(applicationId, params: any = {}) {
    return this.service.create(`applications/${applicationId}/subscription-orders/`, params);
  }

  /**
   * @name getApplicationSubscriptionOrders
   * @desc Queries getApplicationSubscriptionOrders from server
   * @param applicationId
   * @param params (optional)
   * @returns the promise of getApplicationSubscriptionOrders
   */
  getApplicationSubscriptionOrders(applicationId, params: any = {}) {
    return this.service.getAllCustom(`applications/${applicationId}/subscription-orders/`, params);
  }

  /**
   * @name getApplicationSubscriptionOrder
   * @desc Queries getApplicationSubscriptionOrder from server
   * @param applicationId
   * @param id
   * @returns the promise of getApplicationSubscriptionOrder
   */
  getApplicationSubscriptionOrder(applicationId, id) {
    return this.service.getOne(`applications/${applicationId}/subscription-orders/`, id);
  }

  /**
   * @name updateApplicationSubscriptionOrder
   * @desc Queries updateApplicationSubscriptionOrder from server
   * @param applicationId
   * @param id
   * @param params (optional)
   * @returns the promise of updateApplicationSubscriptionOrder
   */
  updateApplicationSubscriptionOrder(applicationId, id, params: any = {}) {
    return this.service.update(`applications/${applicationId}/subscription-orders/`, id, params);
  }

  /**
   * @name cloneApplicationSubscriptionOrder
   * @desc Queries cloneApplicationSubscriptionOrder from server
   * @param applicationId
   * @param id
   * @returns the promise of cloneApplicationSubscriptionOrder
   */
  cloneApplicationSubscriptionOrder(applicationId, id) {
    return this.service.create(`applications/${applicationId}/subscription-orders/${id}/clone/`);
  }

  /**
   * @name deleteApplicationSubscriptionOrder
   * @desc Queries deleteApplicationSubscriptionOrder from server
   * @param applicationId
   * @param id
   * @returns the promise of deleteApplicationSubscriptionOrder
   */
  deleteApplicationSubscriptionOrder(applicationId, id) {
    return this.service.remove(`applications/${applicationId}/subscription-orders/`, id);

  }

}

import { Injectable } from '@angular/core';
import { isUndefined } from 'util';
import { isDefined } from '@angular/compiler/src/util';
import { GenericService } from 'src/app/services/generic.service';

@Injectable({
  providedIn: 'root'
})
export class ApplicationsShippingsService {

  constructor(private service: GenericService) { }

  read(applicationId, id) {
    return this.getApplicationShipping(applicationId, id);
  }

  list(applicationId, params: any = {}) {
    return this.getApplicationShippings(applicationId, params);
  }

  save(applicationId, productData) {
    return this.saveApplicationShipping(applicationId, productData);
  }

  delete(applicationId, id){
    return this.deleteApplicationShipping(applicationId,id);
  }


  /**
 * @name _buildFormData
 * @desc build FormData object to send on the request to the server
 * @param params (optional)
 * @returns the object of FormData
 */
  _buildFormData(params) {
    let fd = new FormData();

    if (isDefined(params.name)) {
      fd.append('name', params.name);
    }
    if (isDefined(params.price)) {
      fd.append('price', params.price);
    }

    return fd;
  }

  /**
   * @name createApplicationShipping
   * @desc Queries createApplicationShipping from server
   * @param applicationId
   * @param params (optional)
   * @returns the promise of createApplicationShipping
   */
  createApplicationShipping(applicationId, params: any = {}) {
    // let fd = this._buildFormData(params);
    return this.service.create(`applications/${applicationId}/shippings/`, params);
  }

  /**
   * @name getApplicationShippings
   * @desc Queries getApplicationShippings from server
   * @param applicationId
   * @param params (optional)
   * @returns the promise of getApplicationShippings
   */
  getApplicationShippings(applicationId, params: any = {}) {
    return this.service.getAllCustom(`applications/${applicationId}/shippings/`, params);
  }

  /**
   * @name getApplicationShipping
   * @desc Queries getApplicationShipping from server
   * @param applicationId
   * @param id
   * @returns the promise of getApplicationShipping
   */
  getApplicationShipping(applicationId, id) {

    return this.service.getOne(`applications/${applicationId}/shippings/`, id);
  }

  /**
   * @name updateApplicationShipping
   * @desc Queries updateApplicationShipping from server
   * @param applicationId
   * @param id
   * @param params (optional)
   * @returns the promise of updateApplicationShipping
   */
  updateApplicationShipping(applicationId, id, params: any = {}) {
    // let fd = this._buildFormData(params);
    return this.service.update(`applications/${applicationId}/shippings/`, id, params);
  }

  /**
   * @name cloneApplicationShipping
   * @desc Queries cloneApplicationShipping from server
   * @param applicationId
   * @param id
   * @returns the promise of cloneApplicationShipping
   */
  cloneApplicationShipping(applicationId, id) {
    return this.service.create(`applications/${applicationId}/shippings/${id}/clone/`);
  }

  /**
   * @name deleteApplicationShipping
   * @desc Queries deleteApplicationShipping from server
   * @param applicationId
   * @param id
   * @returns the promise of deleteApplicationShipping
   */
  deleteApplicationShipping(applicationId, id) {
    return this.service.remove(`applications/${applicationId}/shippings/`, id);
  }

  /**
   * @name saveApplicationShipping
   * @desc Queries saveApplicationShipping from server
   * @param applicationId
   * @param params (optional)
   * @returns the promise of saveApplicationShipping
   */
  saveApplicationShipping(applicationId, productData) {
    let productId = productData.id;

    if (productId==null) {
      return this.createApplicationShipping(applicationId, productData);
    } else {
      return this.updateApplicationShipping(applicationId, productId, productData);
    }
  }
}


import { Injectable } from '@angular/core';
import { isDefined } from '@angular/compiler/src/util';
import { isUndefined } from 'util';
import { GenericService } from '../generic.service';

@Injectable({
  providedIn: 'root'
})
export class ApplicationsWebhooksService {
  constructor(private service: GenericService) { }

  read(applicationId, id) {
    return this.getApplicationWebHook(applicationId, id);
  }

  list(applicationId, params: any = {}) {
    return this.getApplicationWebHooks(applicationId, params);
  }


  save(applicationId, productData) {
    return this.saveApplicationWebHook(applicationId, productData);
  }

  delete(applicationId, id){
    return this.deleteApplicationWebHook(applicationId,id);
  }



  /**
     * @name getWebHookTypeChoices
     * @desc Queries getWebHookTypeChoices from server
     * @returns the promise of getWebHookTypeChoices
     */
     getWebHookTypeChoices () {
      return [
        {name: 'Sale Completed', id: 'SC'},
      ];
    }

  /**
 * @name _buildFormData
 * @desc build FormData object to send on the request to the server
 * @param params (optional)
 * @returns the object of FormData
 */
  _buildFormData(params) {
    let fd = new FormData();

    if (isDefined(params.name)) {
      fd.append('name', params.name);
    }
    if (isDefined(params.web_hook_type)) {
      fd.append('web_hook_type', params.web_hook_type);
    }
    if (isDefined(params.url)) {
      fd.append('url', params.url);
    }


    return fd;
  }

  /**
   * @name createApplicationWebHook
   * @desc Queries createApplicationWebHook from server
   * @param applicationId
   * @param params (optional)
   * @returns the promise of createApplicationWebHook
   */
  createApplicationWebHook(applicationId, params: any = {}) {
    // let fd = this._buildFormData(params);
    return this.service.create(`applications/${applicationId}/web-hooks/`, params);
  }

  /**
   * @name getApplicationWebHooks
   * @desc Queries getApplicationWebHooks from server
   * @param applicationId
   * @param params (optional)
   * @returns the promise of getApplicationWebHooks
   */
  getApplicationWebHooks(applicationId, params: any = {}) {
    return this.service.getAllCustom(`applications/${applicationId}/web-hooks/`, params);
  }

  /**
   * @name getApplicationWebHook
   * @desc Queries getApplicationWebHook from server
   * @param applicationId
   * @param id
   * @returns the promise of getApplicationWebHook
   */
  getApplicationWebHook(applicationId, id) {
    
    return this.service.getOne(`applications/${applicationId}/web-hooks/`, id);
  }

  /**
   * @name updateApplicationWebHook
   * @desc Queries updateApplicationWebHook from server
   * @param applicationId
   * @param id
   * @param params (optional)
   * @returns the promise of updateApplicationWebHook
   */
  updateApplicationWebHook(applicationId, id, params: any = {}) {
    // let fd = this._buildFormData(params);
    return this.service.update(`applications/${applicationId}/web-hooks/`, id, params);
  }

  /**
   * @name cloneApplicationWebHook
   * @desc Queries cloneApplicationWebHook from server
   * @param applicationId
   * @param id
   * @returns the promise of cloneApplicationWebHook
   */
  cloneApplicationWebHook(applicationId, id) {
    return this.service.create(`applications/${applicationId}/web-hooks/${id}/clone/`);
  }

  /**
   * @name deleteApplicationWebHook
   * @desc Queries deleteApplicationWebHook from server
   * @param applicationId
   * @param id
   * @returns the promise of deleteApplicationWebHook
   */
  deleteApplicationWebHook(applicationId, id) {
    return this.service.remove(`applications/${applicationId}/web-hooks/`, id);
  }

  /**
   * @name saveApplicationWebHook
   * @desc Queries saveApplicationWebHook from server
   * @param applicationId
   * @param params (optional)
   * @returns the promise of saveApplicationWebHook
   */
  saveApplicationWebHook(applicationId, productData) {
    let productId = productData.id;

    if (productId==null) {
      return this.createApplicationWebHook(applicationId, productData);
    } else {
      return this.updateApplicationWebHook(applicationId, productId, productData);
    }
  }
}

import { Injectable } from '@angular/core';
import { HttpClientModule, HttpClient, HttpHeaders, HttpParams, HttpEvent, HttpHandler, HttpInterceptor, HttpRequest } from '@angular/common/http';
import { Observable } from 'rxjs';

import { tap } from 'rxjs/operators';
import { Constants } from 'src/app/constants';

@Injectable({
  providedIn: 'root'
})
export class GenericService {

  public data: any;
  public name: any;
  public resource: string;
  private api_url = Constants.settings.API.BASE_URL;
  private httpOptions: any = {};
  list: Observable<any>;
 

  constructor(private http: HttpClient) {

    this.httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': 'Token ' + localStorage.getItem('MC2PAuthToken')
      })
    };
   
  }
  // ---------------Create Item--------------------
  public create(resource: string, data?: any): Observable<any> {
    return this.http.post<any>(this.api_url + resource, data, this.httpOptions);
  }

  // ---------------Update Item --------------------
  public update(resource: string, id: string, data: any): Observable<any> {
    return this.http.patch<any>(this.api_url + resource + id + '/', data, this.httpOptions);
  }

  // ---------------Remove Item --------------------
  public remove(resource: string, id: string): Observable<any> {
    return this.http.delete<any>(this.api_url + resource + id, this.httpOptions);
  }

  // ---------------Get All --------------------
  public getAll(resource: string): Observable<any> {
    return this.http.get<any>(this.api_url + resource, this.httpOptions);
    //.pipe( tap(data => data));
  }

  // ---------------Get detail item --------------------
  public getOne(resource: string, id: string): Observable<any> {
    return this.http.get<any>(this.api_url + resource + id, this.httpOptions);
  }

  // ---------------Get custom --------------------
  public getCustom(resource: string, id?: string, end_source?: string, end_id?: string): Observable<any> {
    return this.http.get<any>(this.api_url + resource + id + end_source + end_id, this.httpOptions);
  }


  // ---------------Get All custom --------------------
  public getAllCustom(resource: string, params?: any): Observable<any> {
    if (params) {
      // this.httpOptions.params = new HttpParams().set('params',params);
      this.httpOptions.params = params;
    }
    return this.http.get<any>(this.api_url + resource, this.httpOptions);
  }


}
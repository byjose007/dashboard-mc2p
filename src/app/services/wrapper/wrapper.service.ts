import { Injectable } from '@angular/core';
import { ApplicationsOrdersService } from 'src/app/services/applications/applications-orders.service';
import { ApplicationsSalesService } from 'src/app/services/applications/applications-sales.service';
import { TransactionsComponent } from '../../pages/core/transactions/transactions.component';
import { ApplicationsTransactionsService } from '../applications/applications-transactions.service';

@Injectable({
  providedIn: 'root'
})
export class WrapperService {

  constructor(private ordersService: ApplicationsOrdersService,
    private salesService: ApplicationsSalesService,
    private transactionsService: ApplicationsTransactionsService) { }


  getService(page: string, applicaitonId?, params?) {
    let service: any = {};

    if (page == 'orders') {
      service.read = this.ordersService.getApplicationOrder;
      service.list = this.ordersService.getApplicationOrders;
      service.clone = this.ordersService.cloneApplicationOrder;
      service.delete = this.ordersService.deleteApplicationOrder;
    }
    else if (page == 'transactions') {

      service.read = this.transactionsService.getApplicationTransaction;
      service.list = this.transactionsService.getApplicationTransactions;
    }
    // else if (page == 'subscriptions') {
    //   service.read = TransactionsService.getApplicationSubscriptionTransaction;
    //   service.list = TransactionsService.getDoneApplicationSubscriptionTransactions;
    // }
    // else if (page == 'authorizations') {
    //   service.read = TransactionsService.getApplicationAuthorizationTransaction;
    //   service.list = TransactionsService.getDoneApplicationAuthorizationTransactions;
    // }
    else if (page == 'sales') {
      service.read = this.salesService.getApplicationSale;
      service.list = this.salesService.getApplicationSales;
    }


    /*
    else if (page == 'products') {
      service.read = ProductsService.getApplicationProduct;
      service.list = ProductsService.getApplicationProducts;
      service.clone = ProductsService.cloneApplicationProduct;
      service.delete = ProductsService.deleteApplicationProduct;
      service.save = ProductsService.saveApplicationProduct;
    }
    else if (page == 'plans') {
      service.read = PlansService.getApplicationPlan;
      service.list = PlansService.getApplicationPlans;
      service.clone = PlansService.cloneApplicationPlan;
      service.delete = PlansService.deleteApplicationPlan;
      service.save = PlansService.saveApplicationPlan;
    }
    else if (page == 'coupons') {
      service.read = CouponsService.getApplicationCoupon;
      service.list = CouponsService.getApplicationCoupons;
      service.clone = CouponsService.cloneApplicationCoupon;
      service.delete = CouponsService.deleteApplicationCoupon;
      service.save = CouponsService.saveApplicationCoupon;
    }
    else if (page == 'shippings') {
      service.read = ShippingsService.getApplicationShipping;
      service.list = ShippingsService.getApplicationShippings;
      service.clone = ShippingsService.cloneApplicationShipping;
      service.delete = ShippingsService.deleteApplicationShipping;
      service.save = ShippingsService.saveApplicationShipping;
    }
    else if (page == 'taxes') {
      service.read = TaxesService.getApplicationTax;
      service.list = TaxesService.getApplicationTaxes;
      service.clone = TaxesService.cloneApplicationTax;
      service.delete = TaxesService.deleteApplicationTax;
      service.save = TaxesService.saveApplicationTax;
    }
    else if (page == 'webhooks') {
      service.read = WebHooksService.getApplicationWebHook;
      service.list = WebHooksService.getApplicationWebHooks;
      service.clone = WebHooksService.cloneApplicationWebHook;
      service.delete = WebHooksService.deleteApplicationWebHook;
      service.save = WebHooksService.saveApplicationWebHook;
    }*/
    return service;
  }

}
import { Injectable } from '@angular/core';
import { isDefined } from '@angular/compiler/src/util';

@Injectable({
  providedIn: 'root'
})
export class SearchService {

  constructor() { }


  // initQuery(params) {
  //   let queryDict = {};

  //   forEach(params, function (value, key) {
  //     if (isDefined(value)) {
  //       if (key === 'search' || key === 'date_min' || key === 'date_max') {
  //         queryDict[key] = value;
  //       } else if (key === 'page') {
  //         queryDict[key] = parseInt(value);
  //       } else {
  //         queryDict[key] = { id: value };
  //       }
  //     }
  //   });

  //   return queryDict;
  // }

  buildParamsFromQuery(queryDict) {
    let key: any;
    let params: any = {};

    // Type
    if (isDefined(queryDict.opType) && queryDict.opType && isDefined(queryDict.opType.id)) {
      key = 'order_type';
      params[key] = queryDict.opType.id;
      params.opType = queryDict.opType.id;
    }
    // Currency
    if (isDefined(queryDict.currency) && queryDict.currency && isDefined(queryDict.currency.id)) {
      params.currency = queryDict.currency.id;
      params.currency = queryDict.currency.id;
    }
    // Status
    if (isDefined(queryDict.status) && queryDict.status && isDefined(queryDict.status.id)) {
      params.status = queryDict.status.id;
      params.status = queryDict.status.id;
    }
    // Subscription Status
    if (isDefined(queryDict.subscriptionStatus) && queryDict.subscriptionStatus && isDefined(queryDict.subscriptionStatus.id)) {
      key = 'subscription_status';
      params[key] = queryDict.subscriptionStatus.id;
      params.subscriptionStatus = queryDict.subscriptionStatus.id;
    }
    // Authorization Status
    if (isDefined(queryDict.authorizationStatus) && queryDict.authorizationStatus && isDefined(queryDict.authorizationStatus.id)) {
      key = 'authorization_status';
      params[key] = queryDict.authorizationStatus.id;
      params.authorizationStatus = queryDict.authorizationStatus.id;
    }


      // Search toke
      if (isDefined(queryDict.page)) {
        params.page = queryDict.page;
      }
  
      // Search toke
      if (isDefined(queryDict.search) && queryDict.search != '') {
        params = {};
        params.search = queryDict.search;
      }
      
    // Date range
    if (isDefined(queryDict.date_min) || isDefined(queryDict.date_max)) {
      params = {};
      let date_min: string='';
      let date_max: string='';

      if (isDefined(queryDict.date_min) && !isDefined(queryDict.date_max)) {
        date_min = `${queryDict.date_min.day}/${queryDict.date_min.month}/${queryDict.date_min.year}`;
        //date_max = date_min;
      } else if (!isDefined(queryDict.date_min) && isDefined(queryDict.date_max)) {
        date_max = `${queryDict.date_max.day}/${queryDict.date_max.month}/${queryDict.date_max.year}`;
        //date_min = date_max;
      } else {
        date_max = `${queryDict.date_max.day}/${queryDict.date_max.month}/${queryDict.date_max.year}`;
        date_min = `${queryDict.date_min.day}/${queryDict.date_min.month}/${queryDict.date_min.year}`;
      }

      key = 'date_min';
      params[key] = date_min;
      // params[key] = queryDict.date_min;
      key = 'date_max';
      params[key] = date_max;
      // params[key] = queryDict.date_max;
    }

  

    return params;
  }

}





import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ValidateService {

  MESSAGES:any = {
    minlength: 'This value is not long enough.',
    maxlength: 'This value is too long.',
    email: 'A properly formatted email address is required.',
    required: 'This field is required.'
  };

  MORE_MESSAGES:any = {
    demo: {
      required: 'Here is a sample alternative required message.'
    }
  };

  constructor() { }

    checkMoreMessage (name, error) {
      return (this.MORE_MESSAGES[name] || [])[error] || null;
    }

    validationMessage (field, form, errorBin) {
      let messages = [];
      let errors = form.get(field).errors;
      console.log('validate error: ', errors);

      for (let e in errors) {
        console.log(errors[e]);
        if (errors[e]) {
          let specialMessage = this.checkMoreMessage(field, e);
          if (specialMessage) {
            messages.push(specialMessage);
          }
          else if (this.MESSAGES[e]) {
            messages.push(this.MESSAGES[e]);
          }
          else {
            messages.push('Error: ' + e);
          }
        }
      }

      let dedupedMessages = [];
      messages.forEach( el => {
        if (dedupedMessages.indexOf(el) === -1) {
          dedupedMessages.push(el);
        }
      });

      if (errorBin) {
        errorBin[field] = dedupedMessages;
      }
    }

    formValidation (form, errorBin) {
      for (let field in form.value) {
        if (field.substr(0, 1) !== '$') {
          this.validationMessage(field, form, errorBin);
        }
      }
    }

  }


export class Constants {
    public static 'settings' = {
        
        'DEBUG': true,
      'API': {
        'BASE_URL': 'http://localhost:8000/pvt/v1/',

        // 'BASE_URL': 'http://stage.mychoice2pay.com/pvt/v1/',
        'REQUEST_SUFFIX': '/',
        'HTTP_FIELDS': {
          'WITH_CREDENTIALS': false
        }
      },
      'LEGAL_SITE_ID': 1,
      'MC2P_SITE': {
        'BASE_URL': 'https://www.mychoice2pay.com/',
        'LEGAL_PAGES_URL': 'https://www.mychoice2pay.com/#/p/'
      },
      'TOAST': {
        'ALLOW_HTML': true,
        'TIMEOUT': 3000,
        'POSITION_CLASS': 'toast-top-right',
        'PREVENT_DUPLICATES': true,
        'PROGRESS_BAR': true
      },
      'SPINNER': {
        'THEME': {
          'radius': 30,
          'width': 8,
          'length': 16
        }
      },
      'DEFAULT_LANGUAGE': 'es'
        /*endinject*/
      }
    
    constructor() {

    }
}
  



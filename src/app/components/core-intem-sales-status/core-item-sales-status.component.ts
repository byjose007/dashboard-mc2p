import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-core-item-sales-status',
  templateUrl: './core-item-sales-status.component.html',

})
export class CoreItemSalesStatusComponent implements OnInit {
  @Input() item:any;

  constructor() { }

  ngOnInit() {
  }

}

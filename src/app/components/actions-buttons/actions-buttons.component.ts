import { Component, OnInit, Input } from '@angular/core';
import { PrivateService } from 'src/app/services/applications/private.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-actions-buttons',
  templateUrl: './actions-buttons.component.html',
  styleUrls: ['./actions-buttons.component.css']
})
export class ActionsButtonsComponent implements OnInit {
  @Input() dataActions:any;
  @Input() item:any;

  constructor(public privateService: PrivateService,  private router: Router) { }

  ngOnInit() {
  }


  edit(item){
    this.privateService.setItemModel(item);    
    this.dataActions.redirectTo.push(item.id);
    this.router.navigate(this.dataActions.redirectTo);
  }

  delete(item){
    // this.openDialog();
    this.privateService.delete(item,this.dataActions.service);
  }

  clone(){

  }
  

}

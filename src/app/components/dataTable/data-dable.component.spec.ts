import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DataDableComponent } from './data-dable.component';

describe('DataDableComponent', () => {
  let component: DataDableComponent;
  let fixture: ComponentFixture<DataDableComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DataDableComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DataDableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

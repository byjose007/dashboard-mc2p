import { Component, OnInit, Output, Input } from '@angular/core';
import { EventEmitter } from 'events';
import { CurrenciesService } from 'src/app/services/currencies/currencies.service';
import { ApplicationsService } from 'src/app/services/applications/applications.service';
import { PrivateService } from 'src/app/services/applications/private.service';

@Component({
  selector: 'app-filters',
  templateUrl: './filters.component.html',
  styleUrls: ['./filters.component.css']
})
export class FiltersComponent implements OnInit {
  @Input() data: any = {};
  operationTypeChoices: any;
  currencyList: any;
  page:number

  constructor(public privateService: PrivateService,private currenciesService: CurrenciesService, 
    private applicationsService: ApplicationsService) { }

  ngOnInit() {
    this.operationTypeChoices = this.applicationsService.getOperationTypeChoices();
    this.currenciesService.getCurrencies().subscribe(
      (listData: any) => {
        this.currencyList = listData;
      });
  }

  refreshItemsList() {
    this.privateService.refreshItemsList(this.data.service, this.page);
  }

}



import { Component, OnInit, Input } from '@angular/core';
import { PrivateService } from 'src/app/services/applications/private.service';
import { ApplicationsService } from 'src/app/services/applications/applications.service';
import { CurrenciesService } from 'src/app/services/currencies/currencies.service';
import { ApplicationsSalesService } from 'src/app/services/applications/applications-sales.service';


@Component({
  selector: 'app-data-dable',
  templateUrl: './data-dable.component.html',
  styleUrls: ['./data-dable.component.css']
})
export class DataDableComponent implements OnInit {
  @Input() data: any = {};


  page: number = 1;
  operationTypeChoices: any;
  currencyList: any;
  // isCollapsed:boolean=false;
  
  constructor( private applicationsService: ApplicationsService,
    public privateService: PrivateService,private salesService: ApplicationsSalesService, private currenciesService: CurrenciesService) { }

  ngOnInit() {
    console.log(this.data);
    // this.refreshItemsList();
    this.operationTypeChoices = this.applicationsService.getOperationTypeChoices();
    this.currenciesService.getCurrencies().subscribe(
      (listData: any) => {
        this.currencyList = listData;
      });
    //   this.data.rows.forEach(titles => {
    //       for(let title in titles){              
    //           this.titles.push(titles[title]);
    //           this.rows.push(title);
    //       }        
    //   });      
  }

  // refreshItemsList() {
  //   this.privateService.refreshItemsList(this.data.service, this.page);
  // }


}

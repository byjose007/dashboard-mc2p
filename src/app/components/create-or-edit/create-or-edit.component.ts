
import { Component, OnInit, Input, Output, NgModule } from '@angular/core';
import { EventEmitter } from 'protractor';
import { PrivateService } from 'src/app/services/applications/private.service';

import { Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators, FormsModule } from '@angular/forms';



@Component({
  selector: 'app-create-or-edit',
  templateUrl: './create-or-edit.component.html',
})
export class CreateOrEditComponent implements OnInit {
  @Input() data:any = {};
  form:FormGroup;
  constructor(private router: Router, public privateService: PrivateService) {
    
   }
  ngOnInit() {
    this.form = this.data.form;
  }

  save(form) {
    this.privateService.submit(form, this.data.redirectTo, this.data.service);
  }

  back(){
    this.router.navigate(this.data.redirectTo);
  }
}

import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-core-item-sales',
  templateUrl: './core-item-sales.component.html',
  styleUrls: ['./core-item-sales.component.css']
})
export class CoreItemSalesComponent implements OnInit {

  @Input() item:any;

  constructor() { }

  ngOnInit() {
  }

}

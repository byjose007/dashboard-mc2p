import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-core-item',
  templateUrl: './core-item.component.html',
  styleUrls: ['./core-item.component.css']
})
export class CoreIntemComponent implements OnInit {
  @Input() item:any;
  
  constructor() { }

  ngOnInit() {
  }

}

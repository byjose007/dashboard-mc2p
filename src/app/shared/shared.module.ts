import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { CommonModule } from '@angular/common';


import { AsideComponent } from './aside/aside.component';
import { FooterComponent } from './footer/footer.component';
import { RouterModule } from '@angular/router';
import { TopMenuComponent } from 'src/app/shared/top-menu/top-menu.component';
import { SubmenuComponent } from './submenu/submenu.component';
import { PrivateService } from '../services/applications/private.service';

import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { PagesRoutingModule } from '../pages/pages-routing.module';





@NgModule({
    imports: [
        RouterModule,
        CommonModule,
        // PAGES_ROUTES,
        // PagesRoutingModule,
        FormsModule,
        ReactiveFormsModule,
        NgbModule.forRoot(),
      
    ],
    declarations: [
    
        TopMenuComponent,
        AsideComponent,
        SubmenuComponent,
        FooterComponent

       
    ],
    exports: [
        FooterComponent,
        TopMenuComponent,
        AsideComponent,
        SubmenuComponent,
       
        
    ],
    providers: [
        PrivateService,
    ]
})
export class SharedModule { }

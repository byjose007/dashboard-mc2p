import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { PrivateService } from 'src/app/services/applications/private.service';

@Component({
  selector: 'app-top-menu',
  templateUrl: './top-menu.component.html',
  styleUrls: ['./top-menu.component.css']
})
export class TopMenuComponent implements OnInit {
  @Output() header = new EventEmitter();
  submenu: string;
  application: any = {};
  id: any;


  constructor(private route: ActivatedRoute, private router: Router, public privateService: PrivateService) {


  }

  ngOnInit() {

    // this.id = this.privateService.application.id;
    // alert(this.id);
    // console.log("cargando top menu")
    // alert(this.privateService.getIdApplications());

  }



  cargarSubmenu(nombre: string) {
    this.submenu = nombre;
    this.header.emit(nombre);
  }

  toogleSidebar(){
    return true;
  }

  logout(){
    return true;
  }


}
